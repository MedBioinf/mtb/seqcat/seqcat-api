# UTAAdapter

Adapter to convert genomic positions to anminoacid position.

## Installation Guide

In order to use this package, you should install a local verison of UTA and SeqRepo. The **U**niversal **T**ranscript **A**rchive storing Transcripts aligned to sequence references, while SeqRepo strores the biological sequences.

### Install SeqRepo, PyLiftOver and HGVS
1. Install Packages: `sudo apt install -y python3-dev gcc zlib1g-dev tabix`
   (on windows: `python3 -m pip install python-dev-tools --user --upgrade`)
2. Install seqrepo library: `pip install seqrepo`
3. Install additional python librarys: `pip install -r requirements`
4. Create a SeqRepo folder: `sudo mkdir /usr/local/share/seqrepo`
5. Change permissions on that folder: `sudo chown $USER /usr/local/share/seqrepo`
6. Download the SeqRepo data: `seqrepo pull -i 2021-01-29`

### Install UTA as separate docker container
1. Set up a valid UTA version: `uta_v=uta_20210129`
2. Use docker to pull the image: `docker pull biocommons/uta:$uta_v`
3. Create a docker volume: `docker volume create --name=$uta_v`
4. Start a UTA container:
`docker run --name $uta_v -e POSTGRES_PASSWORD=mypw -p 5432:5432 -v $uta_v:/var/lib/postgresql/data biocommons/uta:$uta_v`

### Set up environmental variables

HGVS automatically checks if the environmental variables are set. Otherwise it will try to connect with an online 
version of UTA and downloads sequence data from the NCBI-API (which tends to fail).

1. Location to UTA: `export UTA_DB_URL=postgresql://anonymous:anonymous@localhost:5432/uta/$uta_20210129`
2. Location to SeqRepo: `export HGVS_SEQREPO_DIR=/usr/local/share/seqrepo/latest`

## Examples

You can use the package with command line parameters or import it into another project.

### Command Line Interface

For the command line you can use the following call:
`python3 HgvsConverter.py <chromosome> <position> <reference> <variation> <reference_genome>`

- <chromosome>: Chromosome Number of where the mutation appears.
- <position>: Position where the mutation appears.
- <referece>: Reference Nucleotide of the mutation.
- <variation>: Appearing Nucleotide after the mutation.
- <reference_genome>: Reference genome used for the position. Supports: `hg19` or `hg38`.

Example call: 
`python3 HgvsConverter.py 1 976705 G A hg19`

With the Output:
`NC_000001.11:g.1041325G>A AGRN Gly294Ser`

### Example code to use the adapter in other projects

```
    # Set up a object
    hgvs_obj = HgvsConverter()

    # Convert the position from hg19 to hg38
    if reference_genome.lower() == 'hg19':
        chromosome, position, *_ = hgvs_obj.liftover_hg19_to_hg38(chromosome, position)

    # Generate the correct input format 
    input_var = f'{hgvs_obj.ncbi_dictionary[chromosome]}:g.{position}{reference}>{variant}'

    # Call the function to convert genomic position to aminoacid position
    result = hgvs_obj.genome_to_amino(input_var)

    # Print some results
    print(result['input_data'],
          result['gene_name'],
          hgvs_obj.parse_variant(result['variant']))
```

The results come as a dictionary with different informations. The keys for the dictionary are the following:

- 'input_data': Returns the used input data
- 'parsed_data': Returns the input data parsed by the hgvs-library
- 'validation': Returns if the parsed input was valid
- 'transcripts': Returns all the transcripts found for input data 
- 'gene_name': Returns the gene name of the first transcript
- 'variant': Returns the Aminoacid exchange and the position on the transcript

## Description of input for functions get_protein_sequence and get_protein_sequence_fasta_file

The input has to be of form: "sequence_identifier":"mutation". The sequence identifier can be gene name (e.g TP53 or EGFR) or the transcript (e.g NM_000546.6 or NM_005228.5). Using transcript ensures, that you are using correct wild-type sequence. When using gene name, the information of transcript that was used is included in the response. The mutation can start with "p." or without it and can be included in paranthesis or not. (E.g. all inputs: TP53:M1P, TP53:p.M1P, TP53:(M1P) are valid). Supported mutations are delitions, substitutions, delition-insertions and insertions. Multiple mutations are joined with sign /. The nomenclature is based on https://varnomen.hgvs.org/recommendations/protein/. Stop codon is denoted with Ter or *. 
- Deletions should be denoted like "amino acid with position or range (first amino acid with position to last amino acids with position) that are deleted", follwed by "del" (e.g. TP53:M1del or TP53:M1_E2del).
- Insertions should be denoted like "amino acids with position flanking insertion site (e.g Lys23_Leu24) " "ins" "inserted sequence (e.g. ArgSerGln)"  (e.g TP53:M1_E2insR or TP53:M1_E2insSER to insert S, E and R between 1t and 2nd amino acid.
- Substitution should be denoted by "reference amino acid""position""new amino acid" (e.g. TP53:M1R).
- Deletion-insertion should be denoted like "position amino acid or range of amino acids that are deleted" "delins" "inserted sequence" (e.g. TP53:M1delinsSE, TP53:M1_E2delinsS)
