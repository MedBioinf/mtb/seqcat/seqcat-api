--------
--MANE--
--------
Info: https://www.ncbi.nlm.nih.gov/refseq/MANE/
Downloaded Release: https://ftp.ncbi.nlm.nih.gov/refseq/MANE/MANE_human/release_1.0/

------------
--AACodons--
------------
File generated with help of Wikipedia: https://de.wikipedia.org/wiki/Genetischer_Code#Codon

-----------------------
--CanonicalTranscript--
-----------------------
Somehow downloaded from Ensembl by Tim Tucholski. Origin link unknown.

-------------------
--LiftOver Chains--
-------------------
hg19ToHg38.over.chain.gz:
https://hgdownload.cse.ucsc.edu/goldenPath/hg19/liftOver/

hg38ToHg19.over.chain.gz:
https://hgdownload.cse.ucsc.edu/goldenPath/hg38/liftOver/