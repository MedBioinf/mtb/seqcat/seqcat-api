FROM python:3.9-buster

WORKDIR /app
COPY . /app/

RUN     apt-get -y update
RUN     apt-get -y install curl

RUN     pip install -r requirements
RUN     pip install ./src

RUN     mkdir /seqrepo_data && chmod -R 777 /seqrepo_data
RUN     mkdir /app/commit_data
RUN     curl -X GET -H "PRIVATE-TOKEN: YqQhg-yv_3vG7t9ubDf3" -L "http://gitlab.gwdg.de/api/v4/projects/21640/repository/branches/main" -o /app/commit_data/current_commit.json

EXPOSE 8084
ENTRYPOINT ["python", "/app/src/main/hgvs_converter_flask.py"]