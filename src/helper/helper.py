'''
    TODO: docstring
'''

import json
import pandas as pd
from io import StringIO, BytesIO
import csv

# Simple function to find in two give Codon the pair with just one exchange
# More than one exchange is not supported now
# a: single input codon (this is given by the reference sequence)
# b: multiple codons separated by ';' (this is given through multiple possible exchanges)
def get_codon_single_exchange(a, b):
    '''
        TODO: docstring
    '''

    b_split = b.split(";")

    for b in b_split:
        positions = []
        if len(a) != len(b):
            return -1, -1
        for i, _ in enumerate(a):
            if a[i] != b[i]:
                positions.append(i)

        if len(positions) == 1:
            return f"{a[positions[0]]}>{b[positions[0]]}", positions[0] + 1

    return -1, -1


def extract_commit_data():
    '''
        TODO: docstring
    '''

    with open("/app/commit_data/current_commit.json", "r", encoding='utf-8') as f_r:
        curl_output = f_r.readlines()
        commit_info = json.loads(curl_output[0])

        return [commit_info["commit"]["short_id"],
            commit_info["commit"]["authored_date"].split("T")[0]]

def convert_three_letter_aa(*aa_values):
    '''
        TODO: docstring
    '''

    converting_table = pd.read_csv("res/data/aa_codons.csv", dtype=str)
    converting_table["Three-Letter"] = converting_table["Three-Letter"].str.lower()

    results = []
    for amino in aa_values:
        amino = amino.lower()
        aa_converted = converting_table.loc[converting_table["Three-Letter"] == amino]["One-Letter"].item()
        results.append(aa_converted)

    return results


def write_fasta_file(sequence, input_var, protein_id):
    # based on https://stackoverflow.com/questions/35710361/python-flask-send-file-stringio-blank-files
    proxy = StringIO()
    
    writer = csv.writer(proxy)
    writer.writerow(["> " + input_var + "; " + protein_id])
    pos = 0
    while(pos < len(sequence)):
        writer.writerow([sequence[pos:pos+60]])
        pos = pos+60
    
    mem = BytesIO()
    mem.write(proxy.getvalue().encode())
    mem.seek(0)
    proxy.close()
    return mem


def structure_output_correctly(qid, data, error_message = None):
    '''
    data should be the dictionary with data that needs to be returned
    error_message should be a string or an array of strings
    '''
    status_code = 200
    header = {'qid': qid}
    if error_message is not None and len(error_message) > 0:
        status_code = 400
        header['error_message'] = error_message
    elif len(data) == 0:
        status_code = 204
    header['status_code'] = status_code
    response = {'header': header,
                'data': data}
    return response



