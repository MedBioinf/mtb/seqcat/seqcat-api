import re
import adagenes



def normalize_protein_request(input_var, transcript_handler):
    """
    Normalizes requests on protein level to the notation [HUGO gene symbol]:[amino acid exchange], e.g. 'BRAF:V600E'
    """
    normalized_input = ""

    request_type, groups = adagenes.get_variant_request_type(input_var)
    print(request_type)
    print("groups ", groups)

    if request_type == "refseq_transcript":
        transcript_id = groups[0]
        cdna = groups[1]

        print(transcript_id)
        gene = transcript_handler.get_gene_name_by_transcript(transcript_id)
        print("gene ",gene)

    return normalized_input
