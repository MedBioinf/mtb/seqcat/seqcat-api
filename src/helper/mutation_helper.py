import logging
import pandas as pd
import re

''' 
this script should get the sequence (as a string) and name of mutation
the main function is "mutate"



Possible events:
- substitution: Format: “prefix”“amino_acid”“position”“new_amino_acid”, e.g. p.(Arg54Ser)
- delition (1 or more AAc are deleted): Format: “prefix”“amino_acid(s)+position(s)_deleted”“del”, e.g. p.(Cys76_Glu79del)
- delition-insertion  Format: “prefix”“amino_acid(s)+position(s)_deleted”“delins”“inserted_sequence”, e.g. p.(Arg123_Lys127delinsSerAsp)
    deleted and inserted cannot both be of length 1, because this is then substitution!
    but one AA can be replaced with 2 AA (e.g. p.Cys28delinsTrpVal)
. insertion


'''

THREE_LETTER_COL = "Three-Letter"
ONE_LETTER_COL = "One-Letter"
ERRORS = []


def add_error(error_msg):
    ERRORS.append(error_msg)


def read_converting_table(path):
    '''
    read converting table and preprocess it (so we have only 2 columns: 3 letter code and 1 letter code
    and 3 letter codes are all in upper case, after that 3letter codes are set to be index of this dataframe
    so we will be able to get 1 letter code for given 3 letter code
    '''
    converting_table = pd.read_csv(path, dtype=str)
    converting_table = converting_table.loc[:, (THREE_LETTER_COL, "One-Letter")]
    converting_table[THREE_LETTER_COL] = converting_table[THREE_LETTER_COL].str.upper()
    converting_table = converting_table.set_index(THREE_LETTER_COL)
    # converting_table = converting_table.drop(["START", "STOP"])  # might be the problem!
    # converting_table.loc["TER"] = "*"   # might be the problem!
    return converting_table


def process_first_amino_acid_code(amino_acid: str):
    '''
    for a given amino acid check if it is described using 3 letter codes. If yes, change them to 1 letter code
    otherwise we check that the amino acid is described using only one sign and the sign is in the converting_table
    return the one letter code and length of the amino acid code that was originally used
    this function should be used only for first amino acid in the mutation name
    '''
    amino_acid_upper = amino_acid.upper()
    if (len(amino_acid_upper) == 3):
        return change_3_letter_code_to_1_letter_code(amino_acid_upper), 3
    elif (len(amino_acid_upper) == 1):
        # check that it is one of the accepted signs
        if (not amino_acid_upper in CONVERTING_TABLE.loc[:, ONE_LETTER_COL].values):
            add_error("The 1 letter code for AA is not valid. The code was: " + amino_acid)
        return amino_acid_upper, 1
    else:
        add_error("The length of code for AA is not valid. The code was: " + amino_acid)
        return amino_acid, -1


def change_3_letter_code_to_1_letter_code(amino_acid_code: str):
    '''
    the input should be a 3 letter code!
    '''
    amino_acid_upper = amino_acid_code.upper()
    if (amino_acid_upper in CONVERTING_TABLE.index):
        logging.info("The 3 letter codes for amino acids was changed to 1 letter codes.")
        return CONVERTING_TABLE.loc[amino_acid_upper].values[0]
    else:
        add_error("The 3 letter code for AA is not valid. The code was: " + amino_acid_code)


def check_notation_and_get_1letterAA(amino_acid: str, len_of_aa_code: int, mutation: str):
    '''
    the string amino_acid should contain the len_of_aa_code letters code of amino acid
    example of inputs: (Lys, 3) or (L, 1)
    check that the string really is of such length and that it is valid
    and return 1 letter code of amino acid
    '''
    if (len(amino_acid) != len_of_aa_code):
        add_error("In mutation " + mutation + " the length of amino acid code should always be " + str(
            len_of_aa_code) + " but the amino acid code was: " + amino_acid)
    aa_upper = amino_acid.upper()
    if (len_of_aa_code == 3):
        return change_3_letter_code_to_1_letter_code(aa_upper)
    elif (len_of_aa_code == 1):
        if (not aa_upper in CONVERTING_TABLE.loc[:, ONE_LETTER_COL].values):
            add_error("The invalid 1letter code: " + amino_acid + " in mutation: " + mutation)
        else:
            return aa_upper
    else:
        add_error("This should have happend! The length of mutation is said to be of length " + str(len_of_aa_code))


def parse_insertion(mutation_substring: str, mutation: str, len_of_aa_code: int):
    '''
    if the recognised length of amino acids code is 3 then translate them to 1 letter codes and return the sequence
    if the recognised length of amino acids code is 1 then check if its correct and return the standard 1 letter coded aas
    '''
    if (len(mutation_substring) % len_of_aa_code != 0):
        add_error("The aminio acids codes are not consistent! " + mutation)
    elif (len(mutation_substring) == 0):
        add_error("There is no AA to be inserted!")
    inserted_sequence_1_letter_code = ""
    if (len_of_aa_code == 3):
        stop = int(len(mutation_substring) / len_of_aa_code + 1)
        for i in range(1, stop):
            current_aa = mutation_substring[((i - 1) * len_of_aa_code):(i * len_of_aa_code)]
            current_aa_1letter = check_notation_and_get_1letterAA(current_aa, len_of_aa_code, mutation)
            inserted_sequence_1_letter_code = inserted_sequence_1_letter_code + current_aa_1letter
    elif (len_of_aa_code == 1):
        for char in mutation_substring:
            aa_checked = check_notation_and_get_1letterAA(char, 1, mutation)
            inserted_sequence_1_letter_code = inserted_sequence_1_letter_code + aa_checked
    else:
        add_error("This shouldn't happen! The length of amino acid code seems to be invalid. " + mutation)
    return inserted_sequence_1_letter_code


def get_type_of_mutation_and_parse_mutation(mutation: str):
    '''
    for 1 mutation find out what type it is (is it substitution/delition/insertion/delition-insertion?)
    and parse the mutation (part of this is also translating 3letter codes of AA to 1 letter, if needed)
    '''
    if mutation[0] == "(" and mutation[-1] == ")":
        mutation = mutation[1:-1]
        logging.info("The parentheses were ignored")

    # first character should be character, after that if there is a number, this means 1 letter code is used
    # otherwise 3 letter code is used, so the first part of number should be on 4th place

    # find the first number in mutation name and split the string into 2
    numbers = re.findall(r'\d+', mutation)  # find all substrings representing numbers inside the string (e.g 123, 14)
    if (len(numbers) < 1):
        add_error("Unsupported type of mutation! It is expected that every mutation has at least 1 number!")
    string_aa, mutation_second_part = mutation.split(
        numbers[0])  # before the first number there should be only 1 amino acid

    first_aa, len_of_aa_code = process_first_amino_acid_code(string_aa)
    first_aa_pos = int(numbers[0])
    if (len_of_aa_code == -1):
        # the AA code was incorrrect!
        return pd.Series([mutation, first_aa_pos, first_aa, first_aa_pos, first_aa, "ERROR",
                          [first_aa_pos, first_aa_pos]], index=COLUMNS)

    if (mutation_second_part[0] == "_"):
        # this mutation affects more than just 1 place! after this we expect amino acid code and residue number
        # split the substring based on the second number that we found
        mutation_second_part = mutation_second_part[1:]  # delete "_" sign from the start
        string_2nd_aa, mutation_second_part = mutation_second_part.split(numbers[1])
        second_aa_pos = int(numbers[1])
        second_aa = check_notation_and_get_1letterAA(string_2nd_aa, len_of_aa_code, mutation)

    else:
        second_aa_pos = first_aa_pos
        second_aa = first_aa

    # now the keyword is expected, except in case of substitution, where the string that is left equals
    # to an amino acid code
    if (mutation_second_part[0:3] == "ins"):
        aas_to_be_inserted = parse_insertion(mutation_second_part[3:], mutation, len_of_aa_code)
        # if(first_aa_pos != second_aa_pos -1):
        # add_error("The insertion positions aren't consecutive!")
        return pd.Series([mutation, first_aa_pos, first_aa, second_aa_pos, second_aa, "insertion",
                          [first_aa_pos, second_aa_pos, aas_to_be_inserted]], index=COLUMNS)
    elif (mutation_second_part[0:3] == "del"):
        if (mutation_second_part == "del"):
            return pd.Series([mutation, first_aa_pos, first_aa, second_aa_pos, second_aa, "deletion",
                              [first_aa_pos, second_aa_pos]], index=COLUMNS)

        elif (mutation_second_part[0:6] == "delins"):
            aas_to_be_inserted = parse_insertion(mutation_second_part[6:], mutation, len_of_aa_code)
            return pd.Series([mutation, first_aa_pos, first_aa, second_aa_pos, second_aa, "delins",
                              [first_aa_pos, second_aa_pos, aas_to_be_inserted]], index=COLUMNS)
        else:
            add_error("Invalid input! This is neither delition nor delition-insertion! " + mutation)
    elif (mutation_second_part[0:3] == "dup"):
        add_error("The dupplications aren't supported!")
    else:
        if (mutation_second_part[0] == "["):
            add_error("Repeated sequences aren't supported!")
        elif (len(mutation_second_part) == len_of_aa_code):
            # this is supposed to be substitution
            new_aa = check_notation_and_get_1letterAA(mutation_second_part, len_of_aa_code, mutation)
            return pd.Series(
                [mutation, first_aa_pos, first_aa, first_aa_pos, first_aa, "substitution", [first_aa_pos, new_aa]],
                index=COLUMNS)
        elif (mutation_second_part[len_of_aa_code:len_of_aa_code + 2] == "fs"):
            add_error("The frame shift isn't supported!")
        else:
            # this might happen if you use combination of 3 leter code and 1 letter code with substitution
            add_error("The type of mutation is not recognized!")
    # there was an error - return pd Series, because it is expected
    return pd.Series([mutation, first_aa_pos, first_aa, first_aa_pos, first_aa, "ERROR",
                      [first_aa_pos, first_aa_pos]], index=COLUMNS)


def check_if_amino_acid_is_on_position(amino_acid_code: str, number: int, sequence: str):
    if (sequence[number - 1] != amino_acid_code):
        add_error("Wrong amino acid selected! On the residue number " + str(number) + " there is " + sequence[
            number - 1] + " and not " + amino_acid_code)


def check_corectness_of_positions(mutations_info, len_sequence: int, sequence: str):
    '''
    for pandas data frame mutations_info, check that positions of mutations don't overlap!
    the mutations are already ordered based on the position. So only consecutive mutations have to be checked
    based on length of sequence check that no mutation is happening "outsite" of the sequence
    at the end, check that at the starting and ending positions there are really those
        amino acids that are said to be there based on mutation name
    '''
    first = True
    # be careful index is not based on ordered starting position! but the itterow follows the ordered rows
    for index, row in mutations_info.iterrows():
        if first == True:
            current_start = row["start_res_n"]
            current_end = row["end_res_n"]
            prev_row = row
            first = False
        else:
            prev_start = current_start
            prev_end = current_end
            current_start = row["start_res_n"]
            current_end = row["end_res_n"]
            # current_end is suppose to be smaller than prev_start, as we ordered them with residue number ascending
            if (current_end >= prev_start):
                add_error("Two mutations affect the same residue: " + prev_row["mutation_name"] + " and " + row[
                    "mutation_name"])
            prev_row = row
        # check if ending residue number is smaller than starting
        if (current_end < current_start):
            add_error("The end position of mutation is smaller than staring position: " + row["mutation_name"])

        if (current_end > len_sequence):
            add_error("The end position for mutation is bigger than seuqnece length: " + row["mutation_name"])
            return

            # check that at said positions there are said amino acids
        check_if_amino_acid_is_on_position(row["start_res_AA"], current_start, sequence)
        if (current_end != current_start):
            check_if_amino_acid_is_on_position(row["end_res_AA"], current_end, sequence)
    return


def execute_substitution(array_of_parameters, sequence: str):
    '''
    for the sequnece and array_of_parameters that consists of number of residue, code of the amino acid there and code of new amino acid
    changethe chosne position to new_aa and return updated sequence

    Special case: if the new_aa == "*" then we should delete everything after it
    '''
    position, new_aa = array_of_parameters
    position_in_string = position - 1

    if (new_aa == "*"):
        sequence = sequence[:position_in_string]
    else:
        sequence = sequence[:position_in_string] + new_aa + sequence[
                                                            position_in_string + 1:]  # change the character at position number_in_string
    return sequence


def execute_deletion(array_of_parameters, sequence: str):
    '''
    for the sequnece and array_of_parameters (first_aa_pos, second_aa_pos)
    delete amino acids between first_aa_pos and second_aa_pos
    '''
    first_aa_pos, second_aa_pos = array_of_parameters
    # drop everything between these 2 positions! Don't forget that the indexes in strings starts from 0
    new_sequence = sequence[:(first_aa_pos - 1)] + sequence[second_aa_pos:]
    print("Previous length of sequnce was ", len(sequence), " after deletion it is", len(new_sequence))
    return new_sequence


def execute_delins(array_of_parameters, sequence: str):
    '''
    for the sequence and array of parameters (first_aa_pos, second_aa_pos,  inserted_sequence)
    delete the amino acids between (including) first_aa_pos and second_aa_pos and insert there sequence inserted_sequence
    '''
    first_aa_pos, second_aa_pos, inserted_sequence = array_of_parameters
    new_sequence = sequence[:(first_aa_pos - 1)] + inserted_sequence + sequence[
                                                                       second_aa_pos:]  # positions are 1 more than indexes!
    return new_sequence


def execute_insertion(array_of_parameters, sequence: str):
    '''
    for the sequence and array of parameters (first_aa_pos, second_aa_pos,  inserted_sequence)
    insert inserted_sequence between first_aa_pos and second_aa_pos
    '''
    first_aa_pos, second_aa_pos, inserted_sequence = array_of_parameters
    if (second_aa_pos != first_aa_pos + 1):
        add_error(
            "The positions for insertion should be consecutive! But they were: " + str(first_aa_pos) + " and " + str(
                second_aa_pos))
    new_sequence = sequence[:first_aa_pos] + inserted_sequence + sequence[
                                                                 second_aa_pos - 1:]  # positions are 1 more than indexes!
    print("Previous length of sequnce was ", len(sequence), " after insertion it is", len(new_sequence))
    return new_sequence


def parse_mutation_name(mutation_name: str):
    '''
    for input (mutation name) recognise the parts that correspond to different mutations and return array of them and infornation about them, ordered by
        residue number
    the mutation name can start with "p.", but doesnt have to! around the mutation(s) there can be paranthesis
    the amino acids can be indicated with 1 character code or with multiple character code!
    every mutation includes at least one number. if there are two numbers, they have to be connected with _
    '''

    if mutation_name.startswith("p."):
        mutation_name = mutation_name[2:]
    if (mutation_name.startswith("(") and mutation_name[-1] == ")"):
        mutation_name = mutation_name[1:-1]
    if "?" in mutation_name:
        return "? mark as a mutation not suported!"

    # multiple mutatations are joined together with the sign /
    mutation_list = mutation_name.split("/")
    # print(mutation_list)

    # for every mutation get the mutation name, starting residue number and amino acid there, the last residue number
    # from original sequence that was effected and AA there and type of event

    mutations_info = pd.DataFrame([], columns=COLUMNS)
    for mutation in mutation_list:
        mutations_info = pd.concat([mutations_info, get_type_of_mutation_and_parse_mutation(mutation).to_frame().T],
                                   axis=0, join="outer", ignore_index=True)
        # mutations_info= mutations_info.append(get_type_of_mutation(mutation), ignore_index=True)  # deprecated
    # order the mutations based on the starting position, where the first line contains the mutation occuring at the latest residue
    mutations_info = mutations_info.sort_values(by=['start_res_n'], ascending=False)
    return mutations_info


def mutate(sequence, mutation_name):
    '''
    for wild type sequence and mutation name return mutated sequence and array of errors
    if there are errors, wild type sequence is returned
    '''
    # get the information (e.g starting and ending positions, type of mutation
    # and parameters needed based on type of mutation) about mutations from mutation_name
    ERRORS.clear()
    mutations_info = parse_mutation_name(mutation_name)
    if (len(ERRORS) == 0):
        # check that all positions are reasonable
        # ( end position < start position or 2 mutation at the same residue or position > length of sequence)
        check_corectness_of_positions(mutations_info, len(sequence), sequence)
    if (len(ERRORS) == 0):
        # execute mutations
        for index, row in mutations_info.iterrows():
            #print(row["event"])
            # based on the event code saved in the "event" column in mutations_info the corresponding
            # function for executing the mutation is called
            # for example, for the substitution, the function execute_substitution is called
            # in general function with can be called with: globals()['sampleFunc']('sample arg')
            # based on: https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-by-using-its-name-a-string
            sequence = globals()['execute_' + row["event"]](row["processed_info"], sequence)
    return sequence, ERRORS


CONVERTING_TABLE = read_converting_table("res/data/aa_codons.csv")
COLUMNS = ["mutation_name", "start_res_n", "start_res_AA", "end_res_n", "end_res_AA", "event", "processed_info"]
