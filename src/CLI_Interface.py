import argparse
import os
from HgvsConverter import HgvsConverter

# TODO
# Not really functional now with the addition of more functions
# Better do a starting parameter to call a specific function
# --genome_to_aa <args>
# --aa_to_genome <args>
# --liftover <args>

def main():
    parser = argparse.ArgumentParser(description='Converts the position from a genomic coordinate '
                                                 'to a protein coordinate.')
    parser.add_argument('chromosome', help='Chromosome Number of where the mutation occurs.',
                        type=int)
    parser.add_argument('position', help='Position where the mutation appears.',
                        type=int)
    parser.add_argument('reference_nucleotide', help='Reference Nucleotide of the mutation.',
                        type=str)
    parser.add_argument('variant_nucleotide', help='Appearing Nucleotide after the mutation.',
                        type=str)
    parser.add_argument('reference_genome', help='Reference genome used for the position. '
                                                 'Supports hg19 or hg38', type=str)

    args = parser.parse_args()

    chromosome = args.chromosome
    position = args.position
    reference = args.reference_nucleotide
    variant = args.variant_nucleotide
    reference_genome = args.reference_genome

    hgvs_obj = HgvsConverter()

    if reference_genome.lower() == 'hg19':
        chromosome, position, *_ = hgvs_obj.liftover_hg19_to_hg38(chromosome, position)
    else:
        chromosome = f"chr{chromosome}"

    input_var = f'{hgvs_obj.ncbi_dictionary[chromosome]}:g.{position}{reference}>{variant}'
    results = hgvs_obj.genome_to_amino(input_var)

    for result in results:
        print(result['input_data'],
              result['gene_name'],
              # result['gene_name_transcripts'],
              result['transcript'],
              result['transcript_info'],
              result['variant_exchange'],
              hgvs_obj.parse_variant(result['variant_exchange']))


def test_fun(local_connection=False):
    # Generate Test Data
    test_data = {
        'ARGS_REF_GENOME': 'hg19',
        'CHROMOSOME': 1,
        'POSITION': 976705,
        'REFERENCE': 'G',
        'VARIATION': 'A',
        'LOCAL_DB': 'postgresql://anonymous@localhost/uta/uta_20180821'
    }

    # Connect to Database
    if not local_connection:
        hgvs_obj = HgvsConverter()
    else:
        hgvs_obj = HgvsConverter(connection_string=test_data['LOCAL_DB'])

    # Convert hg19 to hg38
    if test_data['ARGS_REF_GENOME'].lower() == 'hg19':
        # print(f"Liftover from: chr{test_data['CHROMOSOME']}, {test_data['POSITION']} to")
        # print(hgvs_obj.liftover_hg19_to_hg38(test_data['CHROMOSOME'], test_data['POSITION']))
        test_data['CHROMOSOME'], test_data['POSITION'], *_ = hgvs_obj.liftover_hg19_to_hg38(test_data['CHROMOSOME'],
                                                                                            test_data['POSITION'])
        test_data['ARGS_REF_GENOME'] = 'hg38'

    # Create the input string
    input_var = f'{hgvs_obj.ncbi_dictionary[test_data["CHROMOSOME"]]}:g.{test_data["POSITION"]}' \
                f'{test_data["REFERENCE"]}>{test_data["VARIATION"]}'

    results = hgvs_obj.genome_to_amino(input_var)

    for result in results:
        print(result['input_data'],
              result['gene_name'],
              result['transcripts'],
              hgvs_obj.parse_variant(result['variant']))


if __name__ == '__main__':
    # Set up environment variables - should be done manually by the user
    os.environ['HGVS_SEQREPO_DIR'] = '/usr/local/share/seqrepo/2018-11-26'
    os.environ['UTA_DB_URL'] = 'postgresql://anonymous:anonymous@localhost:5432/uta/uta_20180821'

    main()
    # test_fun(local_connection=True)
