"""
    TODO: Module docstring
"""
import copy
import json
import os
import requests
import time
from flask import Flask, request, send_from_directory, send_file, jsonify
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
import adagenes

from hgvs_converter import HgvsConverter
from hgvs_wrapper.hgvs_liftover import HgvsLiftOver
from hgvs_wrapper.hgvs_handler import HGVSHandler
from hgvs_wrapper.hgvs_amino_acid_info import AminoAcidInfo
from hgvs_wrapper.hgvs_gene_normalizer import GeneNormalizer
from hgvs_wrapper.hgvs_vcf_converter import VcfConverter
import exceptions.ExceptionClasses as ExceptionClasses
from helper.helper import extract_commit_data, write_fasta_file, structure_output_correctly
import helper.mutation_helper as mutation_helper


# Set up environment variables to locate the uta-psql databse and the seqrepo files
SEQREPO_ENVIRON = "HGVS_SEQREPO_DIR"
UTA_ENVIRON = "UTA_DB_URL"
if os.environ.get(SEQREPO_ENVIRON) is None:
    os.environ[SEQREPO_ENVIRON] = "/seqrepo_data/2021-01-29"
if os.environ.get(UTA_ENVIRON) is None:
    os.environ[UTA_ENVIRON] = "postgresql://anonymous:@uta_database:5432/uta/uta_20210129"

print("Init HGVS Objects...")
HGVS_OBJ_HG38 = HgvsConverter(reference_genome="GRCh38",connection_string=os.getenv(UTA_ENVIRON))
HGVS_OBJ_HG19 = HgvsConverter(reference_genome="GRCh37",connection_string=os.getenv(UTA_ENVIRON))


print("Init Liftover Objects...")
LIFTOVER_OBJ_HG19 = HgvsLiftOver(chain_path="res/data/hg19ToHg38.over.chain.gz")
LIFTOVER_OBJ_HG38 = HgvsLiftOver(chain_path="res/data/hg38ToHg19.over.chain.gz")
LIFTOVER_OBJ_T2T_TO_HG38 = HgvsLiftOver(chain_path="res/data/hs1ToHg38.over.chain.gz")
LIFTOVER_OBJ_HG38_TO_T2T = HgvsLiftOver(chain_path="res/data/hg38ToHs1.over.chain.gz")
LIFTOVER_OBJ_T2T_TO_HG19 = HgvsLiftOver(chain_path="res/data/hs1ToHg19.over.chain.gz")
LIFTOVER_OBJ_HG19_TO_T2T = HgvsLiftOver(chain_path="res/data/hg19ToHs1.over.chain.gz")

print("Init AminoAcidInfo Object...")
aa_info = AminoAcidInfo()

print("Init GeneNormalizer Object...")
gene_normalizer = GeneNormalizer()


# TODO Export to Config File
HOST_IP = "0.0.0.0"
HOST_PORT = "8084"
GITLAB_URI = "http://gitlab.gwdg.de/api/v4/projects/21640/repository/branches/main"

SERVICE_NAME = "/CCS/"
SERVICE_VERSION = "v1/"

ENDPOINT_PREFIX = f"{SERVICE_NAME}{SERVICE_VERSION}"


def set_up_app():
    # Create Flask Object
    app = Flask(__name__)

    # Set up CORS (Cross Origin Resource Sharing)
    CORS(app, resources={r"/*": {"origins": "*"}})

    # Set up SwaggerUI
    SWAGGER_URL = "/CCS/doc/"
    API_URL = "/CCS/openapi/"
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(SWAGGER_URL, API_URL, config={"app_name": "CCS-API"})
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

    return app


def set_reference_genome(reference):
    if reference.lower() == "hg38":
        HGVS_OBJ = HGVS_OBJ_HG38
    elif reference.lower() == "hg19":
        HGVS_OBJ = HGVS_OBJ_HG19
    else:
        raise ExceptionClasses.NotSupportedReferenceGenome
    return HGVS_OBJ


def set_liftover(reference, alt_liftover):
    if (reference.lower() == "hg38") and (alt_liftover.lower() == "hg19"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_HG38
    elif (reference.lower() == "hg19") and (alt_liftover.lower() == "hg38"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_HG19
    elif (reference.lower() == "t2t") and (alt_liftover.lower() == "hg38"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_T2T_TO_HG38
    elif (reference.lower() == "hg38") and (alt_liftover.lower() == "t2t"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_HG38_TO_T2T
    elif (reference.lower() == "t2t") and (alt_liftover.lower() == "hg19"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_T2T_TO_HG19
    elif (reference.lower() == "hg19") and (alt_liftover.lower() == "t2t"):
        LIFTOVER_OBJ = LIFTOVER_OBJ_HG19_TO_T2T
    else:
        raise ExceptionClasses.NotSupportedReferenceGenome
    return LIFTOVER_OBJ


def callHGVSFunction(input_variants, hgvs_function, kwargs={}):
    start = time.time()
    function_handler = HGVSHandler(input_variants, hgvs_function, kwargs)
    service_result = function_handler.batchProcessing()
    end = time.time()

    print(end - start)
    print(service_result)

    return service_result


if __name__ == "__main__":
    app = set_up_app()

    # Set up Routes
    # Route LiftOver
    @app.route(f"{ENDPOINT_PREFIX}liftover/<genome_from_to>/<input_var>/")
    def liftover(genome_from_to, input_var):
        """
        Liftover service converting genomic positions between reference genomes hg19/GRCh37, hg38/GRCh38 and T2T-CHM13

        :param genome_from_to: Source and target reference genome (hg19/hg38/t2t), e.g. hg38:t2t
        :param input_var: Comma-separated list of genomic positions, chr7:140753336
        :return:
        """
        vars = input_var.split(",")
        input_vars_normalized = ""
        for var in vars:
            request_type, groups = adagenes.get_variant_request_type(var)
            var = adagenes.normalize_dna_identifier_position(var,add_refseq=False)
            input_vars_normalized += var + ","
        input_vars_normalized = input_vars_normalized.rstrip(",")

        genome_version_from, genome_version_to = genome_from_to.lower().split(":", 1)
        LIFTOVER_OBJ = set_liftover(genome_version_from, genome_version_to)
        # liftover_obj = HgvsLiftOver(genome_version_from, genome_version_to)
        return jsonify(callHGVSFunction(input_vars_normalized, LIFTOVER_OBJ.liftover))

    # Route Genomic To Gene
    @app.route(f"{ENDPOINT_PREFIX}GenomicToGene/<reference>/<input_var>/")
    def genome_to_amino(reference, input_var):
        """
        DNA to protein service

        :param reference: reference genome (hg38 / hg19 / t2t)
        :param input_var: Genomic location in HGVS or VCF notation ('chr7:140753336A>T')
        :return:
        """
        dc = {}
        vars = input_var.split(",")
        input_vars_normalized = ""
        for var in vars:
            request_type, groups = adagenes.get_variant_request_type(input_var)

            if (request_type == "genomic_location_nc") or (request_type == "genomic_location_nc_refseq") \
                  or  (request_type == "genomic_location_refseq") or (request_type == "genomic_location"):
                input_var_norm = adagenes.normalize_dna_identifier(var,add_refseq=True)
                dc[input_var_norm] = var
                input_vars_normalized += input_var_norm + ","
        input_vars_normalized = input_vars_normalized.rstrip(",")

        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_vars_normalized, HGVS_OBJ.genome_to_amino)

        for i, result in enumerate(call):
                qid = result["header"]["qid"]
                result["header"]["qid"] = dc[qid]

        return jsonify(call)

    @app.route(f"{ENDPOINT_PREFIX}GenomicToGeneVCF/<reference>/<input_var>/")
    def genome_to_amino_vcf(reference, input_var):
        """
        TODO: docstring
        """

        HGVS_OBJ = set_reference_genome(reference)
        results = callHGVSFunction(input_var, HGVS_OBJ.genome_to_amino)
        results = jsonify(results)
        results_vcf = VcfConverter().dna_to_prot_make_vcf(results)
        return results_vcf

    @app.route(f"{ENDPOINT_PREFIX}GetSequence/<reference>/<input_var>/")
    def get_sequence(reference, input_var):
        """
        TODO: docstring
        """
        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_var, HGVS_OBJ.get_sequence)
        return jsonify(call)

    # Route Gene To Genomic
    @app.route(f"{ENDPOINT_PREFIX}GeneToGenomic/<reference>/<input_var>/")
    def gene_to_genomic(reference, input_var):
        """
        Protein to DNA service

        :param reference:
        :param input_var:
        :return:
        """
        dc = {}
        HGVS_OBJ = set_reference_genome(reference)
        vars = input_var.split(",")
        input_vars_normalized = ""
        for var in vars:
            request_type, groups = adagenes.get_variant_request_type(var)
            print(request_type)
            if (request_type == "gene_name_aa_exchange") or (request_type == "gene_name_aa_exchange_long")\
                   or (request_type == "gene_name_aa_exchange_refseq") or (request_type == "gene_name_aa_exchange_long_refseq") :
                input_var_norm = adagenes.normalize_protein_identifier(var, target="one-letter", add_refseq=False)
                dc[input_var_norm] = var
                input_vars_normalized += input_var_norm + ","
        input_vars_normalized = input_vars_normalized.rstrip(",")

        call = HGVS_OBJ.amino_to_genome
        call = callHGVSFunction(input_vars_normalized, call)

        for i, result in enumerate(call):
                qid = result["header"]["qid"]
                result["header"]["qid"] = dc[qid]

        return jsonify(call)

    
    @app.route(f"{ENDPOINT_PREFIX}GeneToGenomicVCF/<reference>/<input_var>/")
    def gene_to_genomic_vcf(reference, input_var):
        """
        TODO: docstring
        """
        HGVS_OBJ = set_reference_genome(reference)
        results = callHGVSFunction(input_var, HGVS_OBJ.amino_to_genome)
        results = jsonify(results)
        results_vcf = VcfConverter().prot_to_dna_make_vcf(results)
        return results_vcf


    # Route cDNA To Protein
    @app.route(f"{ENDPOINT_PREFIX}cdnaToGene/<reference>/<input_var>/")
    def cdna_to_gene(reference, input_var):
        """
        Transcript to protein

        :param reference: reference genome (hg19/hg38/t2t)
        :param input_var: Transcript identifier containing a gene name or transcript ID, followed by a cDNA identifier (e.g. 'KMT2A:c.G8819C')
        :return:
        """
        dc = {}
        HGVS_OBJ = set_reference_genome(reference)
        request_type, groups = adagenes.get_variant_request_type(input_var)
        #if request_type == "refseq_transcript":
        #    call = callHGVSFunction(input_var, HGVS_OBJ.cdna_to_amino)
        #    return call
        call = callHGVSFunction(input_var, HGVS_OBJ.cdna_to_amino)

        return jsonify(call)

    # Route Transcript to Genome
    @app.route(f"{ENDPOINT_PREFIX}transcriptToGene/<reference>/<input_var>/")
    def transcript_to_gene(reference, input_var):
        """
        Transcript to DNA

        :param reference:
        :param input_var:
        :return:
        """
        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_var, HGVS_OBJ.transcript_to_gene)
        return jsonify(call)

    # Route Transcripts
    @app.route(f"{ENDPOINT_PREFIX}getTranscripts/<reference>/<input_gene>/")
    def get_transcripts(reference, input_gene):
        """
        TODO: docstring
        """

        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_gene, HGVS_OBJ.get_transcripts)
        return jsonify(call)

    # Route AminoAcidInfo
    @app.route(f"{ENDPOINT_PREFIX}aminoacidInfo/")
    def aminoacidInfo():
        """
        TODO: docstring
        """
        return jsonify(aa_info.get_aa_info())

    # Route AminoAcidStructures
    @app.route(f"{ENDPOINT_PREFIX}aminoacidStructures/")
    def aminoacidStructures():
        """
        TODO: docstring
        """
        return jsonify(aa_info.get_aa_structures())

    # Route FusionsIsInFrame
    @app.route(f"{ENDPOINT_PREFIX}fusionIsInFrame/<reference>/<input_var>/")
    def fusionIsInFrame(reference, input_var):

        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_var, HGVS_OBJ.fusion_is_inframe)
        return jsonify(call)

    # Route Gene Normalizer
    @app.route(f"{ENDPOINT_PREFIX}GeneNormalizer/<input_var>/")
    def geneNormalizer(input_var):
        call = callHGVSFunction(input_var, gene_normalizer.get_all_info)
        return jsonify(call)

    # Route ExonInfo
    @app.route(f"{ENDPOINT_PREFIX}ExonInfo/<reference>/<input_transcript>/")
    def exonInfo(reference, input_transcript):
        """
        TODO: docstring
        """
        HGVS_OBJ = set_reference_genome(reference)
        call = callHGVSFunction(input_transcript, HGVS_OBJ.get_exon_sequences)
        return jsonify(call)

    # Route ReverseComplement
    @app.route(f"{ENDPOINT_PREFIX}ReverseComplement/<conversion_type>/<input_sequence>/")
    def reverseComplement(input_sequence, conversion_type):
        """
        TODO: docstring
        """
        HGVS_OBJ = HGVS_OBJ_HG38
        call = callHGVSFunction(
            input_sequence, HGVS_OBJ.convert_to_reverse_complement, {"conversion_type": conversion_type}
        )
        return jsonify(call)

    @app.route(f"{ENDPOINT_PREFIX}ConvertGenomicSequence/<input_sequence>/")
    def convertGenomicSequence(input_sequence):
        """
        TODO: docstring
        """
        call = callHGVSFunction(input_sequence, HGVS_OBJ_HG38.convert_nucleotide_sequence)
        return jsonify(call)

    # Route GetProteinSequence
    @app.route(f"{ENDPOINT_PREFIX}get_protein_sequence/<input_var>/")
    def get_protein_sequence(input_var):
        """
        Returns the protein sequence for a gene or a single nulceotide variant denoted with GENE:PROTEIN_CHANGE
        """

        elements = input_var.split(",")
        return_dict = {}
        for element in elements:
            if ":" not in element:
                gene_name = element
                mutation = None
            elif element.count(":") == 1:
                gene_name, mutation = element.split(":")
            else:
                return {"errors": "Too many colons in the input_var"}

            # TODO: hardcoded reference genome
            reference = "hg38"
            HGVS_OBJ = set_reference_genome(reference)

            # protein_id, protein_sequence = HGVS_OBJ.get_transcript_sequence(gene_name)
            # if protein_sequence == -1: #this means there is an error, its saved in protein_id
            #    return jsonify([structure_output_correctly(input_var, {}, protein_id)])

            protein_id, protein_sequence, transcript = HGVS_OBJ.get_protein_sequence(gene_name)
            if protein_sequence == -1:  # this means there is an error, its saved in protein_id
                return jsonify([structure_output_correctly(element, {}, protein_id)])

            # mutate the sequence
            if mutation is not None and len(mutation) > 0:
                protein_sequence, errors = mutation_helper.mutate(protein_sequence, mutation)
                if len(errors) != 0:
                    element_return_dict = structure_output_correctly(input_var, {}, errors)
                    return_dict[element] = element_return_dict
                    continue

            gene_name = None
            if transcript is not None:
                gene_name = HGVS_OBJ.dataprovider.get_tx_identity_info(transcript)[-1]
            data = {
                "gene_name": gene_name,
                "protein_id": protein_id,
                "variant": mutation,
                "protein_sequence": protein_sequence,
                "protein_seq_len": len(protein_sequence),
                "transcript": transcript,
            }
            element_return_dict = structure_output_correctly(element, data)
            return_dict[element] = element_return_dict

        return jsonify([return_dict])  # internal_error  when [return_dict]


    @app.route(f"{ENDPOINT_PREFIX}transcriptToGeneId/<input_var>/")
    def transcript_to_hgnc(input_var):
        """
        Returns the gene name for a transcript
        """
        HGVS_OBJ = HGVS_OBJ_HG38
        return jsonify(callHGVSFunction(input_var, HGVS_OBJ.transcript_to_hgnc_info))

    # Route ProteinSequeceFastaFile
    @app.route(f"{ENDPOINT_PREFIX}get_protein_sequence_fasta_file/<input_var>/")
    def get_protein_sequence_fasta_file(input_var):
        """
        TODO: docstring
        """
        if ":" not in input_var:
            gene_name = input_var
            mutation = None
        elif input_var.count(":") == 1:
            gene_name, mutation = input_var.split(":")
        else:
            return {"errors": "Too many colons in the input_var"}

        # TODO: hardcoded reference genome
        reference = "hg38"
        HGVS_OBJ = set_reference_genome(reference)

        protein_id, protein_sequence, transcript = HGVS_OBJ.get_protein_sequence(gene_name)
        if protein_sequence == -1:  # this means there is an error, its saved in protein_id
            return jsonify([structure_output_correctly(input_var, {}, protein_id)])
        # mutate the sequence
        if mutation is not None and len(mutation) > 0:
            protein_sequence, errors = mutation_helper.mutate(protein_sequence, mutation)
            if len(errors) != 0:
                return_dict = structure_output_correctly(input_var, {}, errors)
                return jsonify([return_dict])
        file_name = input_var + ".fasta"
        returned_file = write_fasta_file(protein_sequence, input_var, transcript)
        return send_file(returned_file, as_attachment=True, download_name=file_name, mimetype="fasta")
        # return send_from_directory("/scratch/uta-database/utaadapter/" , file_name)

    # Route Visualize Pathway
    @app.route(f"{ENDPOINT_PREFIX}visualizePathway/<pathway_name>/")
    def visualize_pathway(pathway_name, target_genes=None, target_relations=None, target_generations=None):
        """
        TODO: docstring
        eg. http://localhost:8084/CCS/v1/visualizePathway/hsa05200/?genes=TP53,MDM2&edges=activation,inhibition,?generations=1
        """

        target_genes = request.args.get("genes")
        target_relations = request.args.get("edges")
        target_generations = request.args.get("generations")

        print(target_genes, target_relations, target_generations)

        if target_generations is None:
            target_generations = 3
        target_generations = int(target_generations)

        if target_relations is None:
            target_relations = [
                "activation",
                "inhibition",
                "expression",
                "repression",
                "indirect effect",
                "state change",
                "binding/association",
                "dissociation",
                "missing interaction",
                "phosphorylation",
                "dephosphorylation",
                "glycosylation",
                "ubiquitination",
                "methylation",
            ]
        if type(target_relations) is not list:
            try:
                target_relations = target_relations.split(",")
            except:
                target_relations = [target_relations]

        # we need the xml file to be downloaded first
        if target_genes is None:
            target_genes = HGVS_OBJ_HG38.all_targets(pathway_name)
        if type(target_genes) is not list:
            try:
                target_genes = target_genes.split(",")
            except:
                target_genes = [target_genes]

        print(pathway_name, target_genes, target_relations, target_generations)
        html = HGVS_OBJ_HG38.visualize_pathway(pathway_name, target_genes, target_relations, target_generations)
        return html

    # Route get MetaData
    @app.route(f"{ENDPOINT_PREFIX}info/")
    def get_metadata():
        """
        TODO: docstring
        """
        headers = {"PRIVATE-TOKEN": "YqQhg-yv_3vG7t9ubDf3"}
        metadata = requests.get(GITLAB_URI, headers=headers, timeout=30).json()
        meta_out = {"title": "UTA Adapter"}
        meta_out["newest_commit_id"] = metadata["commit"]["short_id"]
        meta_out["newest_commit_message"] = metadata["commit"]["title"]
        meta_out["newest_commit_date"] = metadata["commit"]["authored_date"].split("T")[0]
        meta_out["data"] = {"uta": {"version": "20210129", "date": "2021-01-29"}}
        meta_out["current_commit_id"], meta_out["current_commit_date"] = extract_commit_data()
        return meta_out

    # Route OpenAPI
    @app.route(f"{SERVICE_NAME}openapi/")
    def get_openapi():
        """
        TODO: docstring
        """
        return send_from_directory(os.getcwd(), "./res/conf/openapi.json")

    # Run Flask application
    app.run(host=HOST_IP, port=HOST_PORT, debug=False)
