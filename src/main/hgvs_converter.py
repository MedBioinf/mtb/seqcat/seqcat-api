"""
This module is the main part of the CCS. It contains most of the logic converting genomic to
transcipt or aminoacid coordinates.
"""

# disable error message
# pylint: disable=E1101

import configparser
import logging

import hgvs.dataproviders.uta
import hgvs.assemblymapper
import hgvs.dataproviders.seqfetcher
import hgvs.parser
import hgvs.variantmapper

from hgvs_wrapper.hgvs_fusion_in_frame import FusionInFrame
from hgvs_wrapper.hgvs_amino_to_genome import AminoToGenome
from hgvs_wrapper.hgvs_genome_to_amino import GenomeToAmino, GenomeSequenceToAmino
from hgvs_wrapper.hgvs_cdna_to_amino import CDNAtoAmino
from hgvs_wrapper.hgvs_cdna_to_genome import CDNAtoGenome
from hgvs_wrapper.hgvs_transcript_handler import TranscriptHandler
from hgvs_wrapper.hgvs_reverse_complement import ReverseComplement
from hgvs_wrapper.pathway_visualizer.hgvs_visualize_pathway import generate_pathway_html, early_process
from hgvs_wrapper.hgvs_gene_normalizer import GeneNormalizer
from hgvs_wrapper.hgvs_get_sequence import GetSequence
from hgvs_wrapper.hgvs_vcf_converter import VcfConverter

from hgvs.exceptions import HGVSDataNotAvailableError

from exceptions.ExceptionClasses import NoTranscriptFoundException
from exceptions.ExceptionClasses import NoSequenceForTranscriptFoundException

from pandas import read_csv
import re

from functools import lru_cache

logging.basicConfig(level="INFO")
CHROM_REFSEQ_PATHS = "res/data/ref_seq_dict"


class HgvsConverter:

    """
    TODO: Module docstring
    """

    # init the HGVS objects that are needed
    def __init__(self, connection_string=None, reference_genome="GRCh38", align_method="splign"):
        # If no connection_string is given, connect to the publicDB
        if connection_string is None:
            self.dataprovider = hgvs.dataproviders.uta.connect()
        else:
            self.dataprovider = hgvs.dataproviders.uta.connect(db_url=connection_string)

        self.transcript_handler = TranscriptHandler(self.dataprovider)

        self.validator = hgvs.validator.Validator(hdp=self.dataprovider)
        self.assemblymapper = hgvs.assemblymapper.AssemblyMapper(
            self.dataprovider, assembly_name=reference_genome, alt_aln_method=align_method
        )
        self.parser = hgvs.parser.Parser()
        self.ncbi_dictionary = dict(self.load_ncbi_chromosome_dictionarys()[reference_genome])
        self.reference_genome = reference_genome
        self.aa_codons = read_csv("res/data/aa_codons.csv")

        self.gene_position = read_csv("res/data/gene_table_for_import.csv")

        self.gnorm = GeneNormalizer()

        self.seqfetcher = hgvs.dataproviders.seqfetcher.SeqFetcher()

        self.align_method = align_method

    @staticmethod
    def load_ncbi_chromosome_dictionarys():
        """
        TODO: Module docstring
        """
        # Dictionary for Chromosome to NCBI RefSeqAccession
        refseq = configparser.ConfigParser()
        refseq.read_file(open(CHROM_REFSEQ_PATHS, encoding="utf-8"))

        return refseq

    def genome_to_amino(self, input_var):
        """
        TODO: Module docstring
        """
        genome_to_amino_obj = GenomeToAmino(
            self.dataprovider,
            self.assemblymapper,
            self.parser,
            self.validator,
            self.ncbi_dictionary,
            self.gene_position,
        )

        try:
            result = genome_to_amino_obj.genome_to_amino(input_var, self.transcript_handler.get_structured_transcripts, self.aa_codons)
        except NoTranscriptFoundException as e:
            return str(e)

        return result

    def transcript_to_gene(self, transcript):
        """
        Transcript to genome

        :param transcript:
        :return:
        """

        # Check how transcript has to be represented
        parsed_start = self.parser.parse_hgvs_variant(transcript)
        var_g = self.assemblymapper.t_to_g(parsed_start)
        var_p = self.assemblymapper.t_to_p(parsed_start)

        print(var_g)
        print(var_p)
        transcript_id = transcript.split(":")[0]
        gene_name = self.dataprovider.get_tx_identity_info(transcript_id)[-1]
        tx_info = self.dataprovider.get_tx_identity_info(transcript_id)

        result = {
            "transcript_id": tx_info[0],
            "gene_name": gene_name,
            "variant_g": f"{tx_info[0]}:{var_g.type}.{var_g.posedit}",
            "variant_p": f"{var_p}"
        }

        return result

    def transcript_to_genome(self, input_var, groups):
        """

        """
        transcript = groups[0]
        cdna = groups[1]

        gene_data = self.transcript_to_hgnc_info(transcript)
        gene = gene_data['hgnc']

        #variant = self.cdna_to_amino(cdna)

        amino_to_genome_obj = AminoToGenome(self.dataprovider, self.assemblymapper, self.ncbi_dictionary)

        try:
            transcript = self.transcript_handler.get_structured_transcripts(gene)["ncbi_mane_transcript"]
        except NoTranscriptFoundException as e:
            return str(e)

        result = amino_to_genome_obj.amino_to_genome(gene, variant, transcript)
        return result

    def amino_to_genome(self, input_var):
        """
        Retrieves information for a protein on DNA level.

        :param input_var: The protein is described by a gene name and amino acid exchange, which are separated by a colon ('BRAF:p.Arg600Glu')
        :return:
        """

        amino_to_genome_obj = AminoToGenome(self.dataprovider, self.assemblymapper, self.ncbi_dictionary)
        gene, variant = input_var.split(":")
        try:
            transcript = self.transcript_handler.get_structured_transcripts(gene)["ncbi_mane_transcript"]
        except NoTranscriptFoundException as e:
            return str(e)

        print(input_var," transcripts ",transcript)
        result = amino_to_genome_obj.amino_to_genome(gene, variant, transcript)

        return result


    def get_sequence(self, input_var):
        """
        TODO: Module docstring
        """
        seqObj = GetSequence(self.seqfetcher, self.ncbi_dictionary)
        result = seqObj.get_sequence(input_var)

        return result

    def cdna_to_amino(self, input_var):
        """
        TODO: Module docstring
        """
        gene, variant = input_var.split(":")

        cNDA_to_amino_obj = CDNAtoAmino(
            self.dataprovider, self.assemblymapper, self.parser, self.validator, self.ncbi_dictionary
        )
        result = cNDA_to_amino_obj.cdna_to_amino(
            gene,
            variant,
            self.get_transcripts,
        )

        return result

    def get_genes_in_range(self, start, stop, chrom):
        """
        TODO: Module docstring
        """

        # "NC_000007.14:g.140753336A>T"
        input_var_start = f"{chrom}:g.{start}del"
        input_var_end = f"{chrom}:g.{stop}del"

        parsed_start = self.parser.parse_hgvs_variant(input_var_start)
        valid_start = self.validator.validate(parsed_start)
        transcripts_start = self.assemblymapper.relevant_transcripts(parsed_start)

        parsed_end = self.parser.parse_hgvs_variant(input_var_end)
        valid_end = self.validator.validate(parsed_end)
        transcripts_end = self.assemblymapper.relevant_transcripts(parsed_end)

        transcripts = transcripts_start + transcripts_end

        results = []
        for transcript in transcripts:
            gene_name = self.dataprovider.get_tx_identity_info(transcript)[-1]
            results.append(gene_name)

        # TODO return for each transcript the possible exchanges
        print(set(results))
        return results

    def get_transcripts(self, gene_name):
        """
        TODO: Module docstring
        """
        return self.transcript_handler.get_structured_transcripts(gene_name)

    def _parse_input_var(self, input_var):
        chromosome = input_var.split(":")[0].lower()
        chromosome = self.ncbi_dictionary[chromosome]

        # Create a dict with the hg19 chromosome identifier
        genomic_pos = int(input_var.split(":")[1])

        return chromosome, genomic_pos

    def _get_transcripts_in_range(self, chromosome, start, stop):
        transcripts_all = self.dataprovider.get_tx_for_region(chromosome, self.align_method, start, stop)
        transcripts = [x[0] for x in transcripts_all]

        return transcripts

    def _get_transcript_information(self, transcript, chromosome):
        info1 = self.dataprovider.get_tx_info(transcript, chromosome, self.align_method)
        info2 = self.dataprovider.get_tx_identity_info(transcript)
        info3 = self.dataprovider.get_tx_exons(transcript, chromosome, self.align_method)

        exon_info = [
            {
                "tx_start_i": exon["tx_start_i"],
                "tx_end_i": exon["tx_end_i"],
                "chromosome_start_i": exon["alt_start_i"],
                "chromosome_end_i": exon["alt_end_i"],
            }
            for exon in info3
        ]

        info_dict = {
            "hgnc": info1["hgnc"],
            "transcript": transcript,
            "strand": info3[0]["alt_strand"],
            "cds_start_i": info1["cds_start_i"],
            "cds_end_i": info1["cds_end_i"],
            "exon_lengths": info2["lengths"],
            "exons": exon_info,
        }

        return info_dict
    
    def _transcript_to_hgnc(self, transcript_id):
        '''
        Example input:
        transcript_id = 'NM_004985.5'
        returns hgnc id and gene name for the transcript
        '''
        if transcript_id.startswith("NM_") and '.' in transcript_id:
            transcript_prefix = transcript_id.split('.')[0]
            transcript_version = int(transcript_id.split('.')[1])
        
            for i in range(transcript_version, 0, -1):
                try:
                    transcript_id = f'{transcript_prefix}.{i}'
                    info = self.dataprovider.get_tx_identity_info(transcript_id)
                    return info['hgnc']
                except: 
                    continue
        
        if transcript_id.startswith("NM_"):
            transcript_prefix = transcript_id
            for i in range(1, 20, 1):
                try:
                    transcript_id = f'{transcript_prefix}.{i}'
                    info = self.dataprovider.get_tx_identity_info(transcript_id)
                    return info['hgnc']
                except: 
                    continue

        try:
            info = self.dataprovider.get_tx_identity_info(transcript_id)
            return info['hgnc']
        except:
            raise Exception(f"Transcript: {transcript_id} not found.")
        
    
    def transcript_to_hgnc_info(self, transcript_id):
        '''
        Example input:
        transcript_id = 'NM_004985.5'
        returns hgnc id and gene name for the transcript
        '''
        hgnc = self._transcript_to_hgnc(transcript_id)
        info = self.gnorm.get_all_info(hgnc)
        transcripts = self.get_transcripts(hgnc)

        ensmbl_mane = transcripts['ensmbl_mane_transcript']['tx_ac'] if transcripts['ensmbl_mane_transcript'] else []
        ncbi_mane = transcripts['ncbi_mane_transcript']['tx_ac'] if transcripts['ncbi_mane_transcript'] else []

        print(hgnc)
        print(info)
        print(ensmbl_mane, ncbi_mane)

        return_dict = {
            'hgnc': info['approved_symbol'],
            'hgnc_id': info['hgnc_id'],
            'ensembl_mane': ensmbl_mane,
            'ncbi_mane': ncbi_mane
        }

        return return_dict

    def fusion_is_inframe(self, breakpoints):

        breakpoint_1, breakpoint_2 = breakpoints.split("-")

        breakpoint_1_chr, breakpoint_1_pos = self._parse_input_var(breakpoint_1)
        breakpoint_2_chr, breakpoint_2_pos = self._parse_input_var(breakpoint_2)

        breakpoint_1_transctipts = self._get_transcripts_in_range(
            breakpoint_1_chr, breakpoint_1_pos, breakpoint_1_pos
        )
        breakpoint_2_transctipts = self._get_transcripts_in_range(
            breakpoint_2_chr, breakpoint_2_pos, breakpoint_2_pos
        )

        #get a good transcript
        if breakpoint_1_transctipts:
            breakpoint_1_transctipt = breakpoint_1_transctipts[0]
        else:
            raise Exception("No transcript found for breakpoint 1.")

        if breakpoint_2_transctipts:
            breakpoint_2_transctipt = breakpoint_2_transctipts[0]
        else:
            raise Exception("No transcript found for breakpoint 2.")

        breakpoint_1_transcript_info = self._get_transcript_information(breakpoint_1_transctipt, breakpoint_1_chr)
        breakpoint_2_transcript_info = self._get_transcript_information(breakpoint_2_transctipt, breakpoint_2_chr)

        fusion_obj = FusionInFrame()
        result = fusion_obj.fusion_is_inframe(
            breakpoint_1,
            breakpoint_1_pos,
            breakpoint_1_transcript_info,
            breakpoint_2,
            breakpoint_2_pos,
            breakpoint_2_transcript_info,
        )
        return result


    def get_protein_sequence(self, gene):
        """
        Retrieves the amino acid sequence of a gene or transcript

        :param gene: HUGO gene symbol ('BRAF') or RefSeq transcript identifier ('NM_000551.4') or sequence ientifier ('NP_003997.1')
        :return:
        """
        if re.match(r"^(NP_).*\d$", gene):
            protein_id = gene
            transcript = None
        else:
            # TODO: CLEAN UP this mess!
            try:
                transcript, transcript_sequence = self.get_transcript_sequence(gene)
            except NoTranscriptFoundException as e:
                return str(e), -1
            if transcript_sequence == -1:
                return transcript, -1

            protein_id = self.dataprovider.get_pro_ac_for_tx_ac(transcript)

            protein_sequence = None
            temp_protein_id = None
            if not protein_id:
                print("Fallback and search all available transcripts")
                transcripts = self.get_transcript_sequence(gene, single_transcript=False)
                for transcript in transcripts["UTA-Transcripts"]:
                    t = transcript[3]
                    protein_id = self.dataprovider.get_pro_ac_for_tx_ac(t)
                    if protein_id and not protein_sequence:
                        temp_protein_id = protein_id
                        protein_sequence = self.dataprovider.get_seq(protein_id)

                if not temp_protein_id:
                    raise Exception("No Protein ID found.")
                protein_id = temp_protein_id

        try:
            protein_sequence = self.dataprovider.get_seq(protein_id)
        except HGVSDataNotAvailableError:
            return "No data available for the Protein ID", -1, None

        return protein_id, protein_sequence, transcript

    def get_transcript_sequence(self, variant, single_transcript=True):
        """
        TODO: Module docstring
        """
        transcript = variant
        if not re.match(r"^(ENST|NM_).*\d$", transcript):
            try:
                transcript = self.get_transcripts(transcript)
                if not single_transcript:
                    return transcript
            except NoTranscriptFoundException as e:
                return str(e), -1
            transcript = transcript["ncbi_mane_transcript"]['tx_ac']

        try:
            start = self.dataprovider.get_tx_identity_info(transcript)[3]
            stop = self.dataprovider.get_tx_identity_info(transcript)[4]
            transcript_sequence = self.dataprovider.get_seq(transcript, start, stop)
        except NoSequenceForTranscriptFoundException as e:
            return str(e), -1

        return transcript, transcript_sequence

    @lru_cache(maxsize=1280)
    def get_exon_sequences(self, gene_name: str):
        """ 
        TODO: Module docstring
        """
        try:
            tx_info = self.transcript_handler.get_all_transcript_info_by_gene_name(gene_name)

            seq = self.transcript_handler.get_sequence_for_transcript(
                tx_info["tx_ncbi_info"]["tx_ac"],
                tx_info["tx_ncbi_info"]["cds_start_i"],
                tx_info["tx_ncbi_info"]["cds_end_i"],
            )
            converter = GenomeSequenceToAmino(seq, self.aa_codons)

            tx_exon_info = self.transcript_handler.get_exon_sequences(
                tx_info["tx_ncbi_exons"], tx_info["tx_ncbi_info"], converter.aa_sequence
            )
        except:
            raise Exception("Something went wrong in getting the exon sequences.")
        return tx_exon_info

    def convert_nucleotide_sequence(self, seq: str) -> str:
        """
        TODO: Module docstring
        """
        ga = GenomeSequenceToAmino(seq, self.aa_codons)
        return ga.get_results()

    def convert_to_reverse_complement(self, seq: str, conversion_type='reverse_complement') -> str:
        """
            Returns the reverse complement of the sequence. If the sequence contains any characters other than A, T, C, G,
            a, t, c, g, n, N, they are replaced with "?".
            :param seq: sequence to be reverse complemented
            :return: reverse complement of the sequence
        """
        rc = ReverseComplement(seq, conversion_type)
        return rc.results

    def visualize_pathway(self, pathway_name, target_genes=None, target_relations=None, target_generations=None):
        '''
        Example input:
        pathway_name = 'hsa05200.xml', target_genes = ['TP53', 'MDM2'], target_relations = ['inhibition'], target_generations = 2
        This will generate a html file with the pathway and the target genes highlighted and for 2 subsequent generations.
        '''
        html = generate_pathway_html(pathway_name, target_genes, target_relations, target_generations)

        return html

    def all_targets(self, pathway_name):
        '''
        Example input:
        pathway_name = 'hsa05200.xml'
        returns all targets(elements) in the pathway
        '''
        print("downloading pathway early")
        targets = early_process(pathway_name)

        return targets


def main():
    #hgvs_obj_hg19 = HgvsConverter(reference_genome="GRCh37")
    hgvs_obj_hg38 = HgvsConverter(reference_genome="GRCh38")

    # x = hgvs_obj_hg38.transcript_to_gene("NM_201628.3:c.1039C>T")
    # print(x)

    x = hgvs_obj_hg38.cdna_to_amino("AKT1:G49A")
    print(x)

    # from hgvs_wrapper.hgvs_handler import HGVSHandler
    # input_variants = "chr7:47384352-chr7:55225355,chr7:47408367-chrX:66863099"
    # hgvs_function = hgvs_obj_hg19.fusion_is_inframe
    # # kwargs = {}
    # handler = HGVSHandler(input_variants, hgvs_function, kwargs)
    # print(handler.batchProcessing())


    # input_variants = "chr7:140753336-chr6:17799397,chr7:140753336-chr1:1397,chr7:140753336-chr6:17799397"
    # hgvs_function = hgvs_obj_hg38.fusion_is_inframe
    # kwargs = {}
    # handler = HGVSHandler(input_variants, hgvs_function, kwargs)
    # print(handler.batchProcessing())

    # Use this to test the hgsv_converter and debug stuff without starting the the FLASK application.
    pass

if __name__ == "__main__":
    main()
