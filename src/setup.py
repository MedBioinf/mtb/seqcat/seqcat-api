import setuptools

setuptools.setup(
    name="UTA-Adapter",
    version="0.0.1",
    author="Kevin Kornrumpf",
    author_email="kevin.kornrumpf@bioinf.med.uni-goettingen.de",
    description="Package to convert genomic data.",
    url="https://gitlab.gwdg.de/UKEB/mtb/onkopus-modules/utaadapter",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
