from InputParser import InputParser

class ProteinParser(InputParser):
    def __init__(self) -> None:
        self.parser_regex = r'.*:(p.)?\w*\d*\w*$'


if __name__ == '__main__':
    gen_parser = ProteinParser()
    gen_parser.parse_input('JAK1:L910P')