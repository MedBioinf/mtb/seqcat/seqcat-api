from InputParser import InputParser


class GenomicParser(InputParser):

    def __init__(self) -> None:
        super().__init__()

        self.parser_regex = r'(chr)?[1-9xy][0-9]?:(g\.)?\d*$'
        self.parser_func = self._parse_genomic

    def _parse_genomic(self, input_var):
        CHROM_INDEX = 0
        POS_INDEX = 1

        input_var_replacement = input_var.replace('g.', '')
        input_var_splitted = input_var_replacement.split(':')

        return {
            'input': input_var,
            'chromosome': input_var_splitted[CHROM_INDEX],
            'position': input_var_splitted[POS_INDEX]
        }


if __name__ == '__main__':
    gen_parser = GenomicParser()
    print(gen_parser.parse_input('chr17:g.7673776'))
