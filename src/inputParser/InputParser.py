import re
import exceptions.ExceptionClasses as ExceptionClasses

class InputParser:
    '''
    A class for parsing and checking input variants.

    Attributes
    ----------
        endpoint_parser: str
            Abbreviation for the regex to parse a input variant.

    Methods
    -------

    parse_input(input_var=""):
        Parses the given input and checks for correctness.
    match_input(self, input_var=""):
        Checks if the given input for correctness.
    '''

    def __init__(self) -> None:
        '''
        Constructor that stores the type of parsing.
        '''

        self.parser_regex = None
        self.parser_func = lambda X: X

        if not self.parser_regex and not self.parser_func:
            raise ExceptionClasses.BadInputException


    def parse_input(self, input_var="") -> str:
        """
        Parses the given input and checks for correctness.

        Parameters
        ----------
            input_var: str
                Input variant to parse and check

        Returns
        -------
            str
        """

        if not self._match_input(input_var):
            raise ExceptionClasses.BadInputException

        parsed_values = self.parser_func(input_var)
        return parsed_values


    def _match_input(self, input_var="") -> bool:
        """
        Checks if the given input for correctness.

        Parameters
        ----------
            input_var: str
                Input variant to parse and check

        Returns
        -------
            str
        """
        return re.match(self.parser_regex, input_var, re.IGNORECASE)


if __name__ == '__main__':
    parser = InputParser()
    parser.parse_input('JAK1:L910P')
