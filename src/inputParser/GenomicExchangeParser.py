from InputParser import InputParser

class GenomicExchangeParser(InputParser):
    def __init__(self) -> None:
        self.parser_regex = r'(chr)?[1-9xy][0-9]?:(g\.)?\d*\w>\w$'


if __name__ == '__main__':
    gen_parser = GenomicExchangeParser()
    gen_parser.parse_input('chr17:g.7673776A>X')