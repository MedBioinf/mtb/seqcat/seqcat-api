class NoTranscriptFoundException(Exception):
    def __init__(self, gene_name) -> None:
        self.gene_name = gene_name

    def __str__(self):
        return f'No transcript found for gene "{self.gene_name}".'

class BadInputException(Exception):
    def __str__(self):
        return "Bad Input Given."

class WrongReferenceGenomeException(Exception):
    def __init__(self, uta_message) -> None:
        self.uta_message = uta_message

    def __str__(self):
        return f'Probably Wrong Reference Allel. {self.uta_message}'

class IntronAreaException(Exception):
    def __str__(self):
        return 'Probably Intron Area'

class NotSupportedReferenceGenome(Exception):
    def __str__(self):
        return "Reference genome not supported. Use either hg19 or hg38."

class NoSequenceForTranscriptFoundException(Exception):
    def __str__(self):
        return "No sequence found for this transcript. Transcript is probably not in HGVS-Database available."

class NoPossibleSNVExchange(Exception):
    def __init__(self, ref, var) -> None:
        self.ref = ref
        self.var = var

    def __str__(self):
        return f"No possible SNV conversion from {self.ref} to {self.var}."
