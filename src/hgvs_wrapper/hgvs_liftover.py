from pyliftover import LiftOver

class HgvsLiftOver:
    def __init__(self, chain_path=None, genome_from=None, genome_to=None):
        if chain_path:
            self.liftover_obj = LiftOver(chain_path)
        else:
            self.liftover_obj = LiftOver(genome_from, genome_to)


    def liftover(self, input_var):
        '''
            TODO: Module docstring
        '''
        # chr17:g.7673776

        chromosome, position = input_var.split(":", 1)
        position = position.replace(".g", "")

        res = self.liftover_obj.convert_coordinate(chromosome, int(position))
        res = res[0]
        return_dict = {'input': input_var,
                        'chromosome': res[0],
                        'position': res[1],
                        'strand': res[2],
                        'score': res[3]}

        return return_dict
