import json
import re
from flask import jsonify

class VcfConverter():
    def __init__(self):
        self.header = ['##fileformat=VCFv4.2',
        '##INFO=<ID=PROT_ALT, Number=1,Type=String, Description="Protein Alteration for this variant">',
        '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO']

    def dna_to_prot_make_vcf(self, input_str) -> str:
        data = json.loads(input_str.get_data())
        print(data)
        lines = self.header
        
        for result in data:
            if result['header']['status_code'] == 200:
                input_var = result['data']['input_data']
                chrom = input_var.split(':')[0]

                rest_data = input_var.split(':')[1]
                regex = re.compile('(\d+)(\w+)>(\w+)')
                parsed_regex = re.search(regex, rest_data)

                pos = parsed_regex.group(1)
                ref = parsed_regex.group(2)
                alt = parsed_regex.group(3)

                gene = result['data']['gene_name']
                prot = result['data']['variant_exchange']

                lines.append(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tPROT_ALT={gene}:{prot}")

        return_dict = {'vcf': '\n'.join(lines)}

        return jsonify(return_dict)

    def prot_to_dna_make_vcf(self, input_str) -> str:
        data = json.loads(input_str.get_data())
        print(data)
        lines = self.header
        
        for result in data:
            if result['header']['status_code'] == 200:
                input_var = result['data'][0]['results_string']
                chrom = f"chr{result['data'][0]['chromosome']}"

                rest_data = input_var.split(':')[1]
                regex = re.compile('(\d+)(\w+)>(\w+)')
                parsed_regex = re.search(regex, rest_data)

                pos = parsed_regex.group(1)
                ref = parsed_regex.group(2)
                alt = parsed_regex.group(3)

                prot = result['data'][0]['input_var']

                lines.append(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tPROT_ALT={prot}")

        return_dict = {'vcf': '\n'.join(lines)}

        return jsonify(return_dict)
