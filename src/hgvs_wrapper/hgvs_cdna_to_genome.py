import re
from pandas import read_csv
import exceptions.ExceptionClasses as ExceptionClasses
from exceptions.ExceptionClasses import NoTranscriptFoundException


class CDNAtoGenome:

    def __init__(self, dataprovider, assemblymapper, parser, validator, ncbi_dict) -> None:
        self.dataprovider = dataprovider
        self.assemblymapper = assemblymapper
        self.parser = parser
        self.validator = validator
        self.ncbi_dictionary = ncbi_dict

    def cdna_to_genome(self,
                      gene: str,
                      input_var: str,
                      get_transcripts) -> dict:
        """
        Converts a cDNA identifier to a protein

        :param gene: HUGO gene symbol or RefSeq transcript identifier
        :param input_var: cDNA identifier
        :param get_transcripts:
        :return:
        """

        transcript = None

        try:
            if gene.startswith("NM_"):
                transcript = self.dataprovider.get_tx_identity_info(gene)['tx_ac']
                input_orig = input_var
                input_var = f'{transcript}:{input_var}'
            else:
                transcripts = get_transcripts(gene)
                transcript = transcripts["ncbi_mane_transcript"]['tx_ac']
                input_orig = input_var
                input_regex = re.search(r"([A-Za-z]*)(\d*)([A-Za-z]*)$", input_var)
                input_var = f'{transcript}:c.{input_regex.group(2)}{input_regex.group(1)}>{input_regex.group(3)}'
        except NoTranscriptFoundException as e:
            return str(e)

        if 'c.' not in input_var:
            input_var = input_var.split(":")[0] + ":c." + input_var.split(":")[1]

        parsed = self.parser.parse_hgvs_variant(input_var)
        print("parsed :", parsed)

        try:
            valid = self.validator.validate(parsed)
        except Exception as exc:
            raise ExceptionClasses.WrongReferenceGenomeException(exc)

        var_p = self.assemblymapper.c_to_p(parsed)

        aa_exchange_three = str(var_p.posedit).strip('()')
        aa_exchange_three_regex = re.findall(r"\D+|\d+", aa_exchange_three)

        ref_aa_three = aa_exchange_three_regex[0]
        pos = aa_exchange_three_regex [1]
        var_aa_three = aa_exchange_three_regex[2]

        aa_codons = read_csv('res/data/aa_codons.csv')

        ref_aa = aa_codons.loc[aa_codons["Three-Letter"] == ref_aa_three]["One-Letter"].item()
        var_aa = "=" if var_aa_three == "=" else aa_codons.loc[aa_codons["Three-Letter"] == var_aa_three]["One-Letter"].item()


        results = {'input_data': input_orig,
                        'parsed_data': str(input_var),
                        'validation': valid,
                        'transcript': transcript,
                        'gene_name': gene,
                        'variant': str(var_p),
                        'variant_exchange_long': aa_exchange_three,
                        'variant_exchange': ref_aa + pos + var_aa}

        # TODO return for each transcript the possible exchanges

        return results
