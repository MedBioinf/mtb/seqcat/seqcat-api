import json
import os
import requests
import re

class AminoAcidInfo:
    def __init__(self, path="./res/data/aa_info.json"):
        self.filepath = path
        self.aa_structures = {}
        self.aa_data = self._data_loader()

    def _data_loader(self):
        aa_data = self._load_aa_info()
        self._load_aa_structures(aa_data)
        return aa_data

    def _load_aa_info(self):
        with open(self.filepath, "r", encoding="utf-8") as file:
            aa_data = json.load(file)
        return aa_data

    def _load_aa_structures(self, aa_data):
        aa_structure_folder = "aa_structures"

        for aa in aa_data:
            aa_name = aa["AminoAcid"]
            try:
                inpath = f"./res/data/{aa_structure_folder}/{aa_name}.svg"
                with open(inpath, "r", newline="", encoding="utf-8") as file:
                    aa_structure = file.read()
                aa_structure = re.sub(r"[\\\"]", "'", aa_structure)
                aa_structure = re.sub(r"[\n\t]", "", aa_structure)
                self.aa_structures[aa_name] = aa_structure
            except FileNotFoundError:
                print(f"File not found: {inpath}")


    def get_aa_structures(self):
        return self.aa_structures

    def get_aa_info(self):
        return self.aa_data


if __name__ == "__main__":
    aa_info = AminoAcidInfo()
    print(aa_info.get_aa_structures())
