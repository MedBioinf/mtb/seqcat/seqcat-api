import pandas as pd


class GeneNormalizer:
    def __init__(self) -> None:
        # Initialise dataframe for conversion
        self._hugo_col = "HGNC ID"
        self._approved_col = "Approved symbol"
        self._approved_name_col = "Approved name"
        self._alias_col = "Alias symbols"
        self._previous_col = "Previous symbols"
        self._chromosome_col = "Chromosome"
        self.converter_df = self._load_dataframe()

    def _load_dataframe(self):
        # Crate dataframe with possible gene name alterations
        converter_df = pd.read_csv("./res/data/gene_name_converter.csv", sep="\t")
        # Previous and Alias can contain multiple genes separated with comma
        # Split them into single genes and expand dataframe for splitted rows
        converter_df[self._previous_col] = converter_df[self._previous_col].str.split(", ")
        converter_df = converter_df.explode(self._previous_col)
        converter_df[self._alias_col] = converter_df[self._alias_col].str.split(", ")
        converter_df = converter_df.explode(self._alias_col)
        # Keep only Status:approved symbols
        converter_df = converter_df[converter_df["Status"] == "Approved"]
        return converter_df

    def is_approved(self, gene):
        # Match if gene is approved symbol
        return (self.converter_df[self._approved_col] == (gene)).any()

    def get_approved_symbol(self, gene):
        # Check if gene is already approved symbol
        if self.is_approved(gene):
            return gene
        # Check if gene has a previous symbol
        if (self.converter_df[self._previous_col] == gene).any():
            hit = self.converter_df[self.converter_df[self._previous_col] == gene]
            return hit[self._approved_col].iloc[0]
        # Check if gene has a alias symbol
        if (self.converter_df[self._alias_col] == gene).any():
            hit = self.converter_df[self.converter_df[self._alias_col] == gene]
            return hit[self._approved_col].iloc[0]
        # If no gene can be mapped return None
        return None

    def get_alias_symbols(self, approved_gene):
        if self.is_approved(approved_gene):
            hit = self.converter_df[self.converter_df[self._approved_col] == approved_gene]
            return hit[self._alias_col].dropna().drop_duplicates().to_list()
        return None

    def get_previous_symbols(self, approved_gene):
        if self.is_approved(approved_gene):
            hit = self.converter_df[self.converter_df[self._approved_col] == approved_gene]
            return hit[self._previous_col].dropna().drop_duplicates().to_list()
        return None

    def get_chromosome(self, approved_gene):
        if self.is_approved(approved_gene):
            hit = self.converter_df[self.converter_df[self._approved_col] == approved_gene]
            return hit[self._chromosome_col].iloc[0]
        return None

    def get_approved_name(self, approved_gene):
        if self.is_approved(approved_gene):
            hit = self.converter_df[self.converter_df[self._approved_col] == approved_gene]
            return hit[self._approved_name_col].iloc[0]
        return None

    def get_hgnc_id(self, approved_gene):
        if self.is_approved(approved_gene):
            hit = self.converter_df[self.converter_df[self._approved_col] == approved_gene]
            return hit[self._hugo_col].iloc[0]
        return None

    def get_all_info(self, gene):
        is_approved = self.is_approved(gene)

        if not is_approved:
            temp_gene = self.get_approved_symbol(gene)
            if not temp_gene:
                raise ValueError(f"No approved symbol found for gene: {gene}")
            gene = temp_gene

        alias = self.get_alias_symbols(gene)
        previous = self.get_previous_symbols(gene)
        chromosome = self.get_chromosome(gene)
        approved_name = self.get_approved_name(gene)
        hgnc_id = self.get_hgnc_id(gene)

        hit = {
            "is_approved": str(is_approved),
            "approved_symbol": gene,
            "alias_symbols": alias,
            "previous_symbols": previous,
            "chromosome": chromosome,
            "approved_name": approved_name,
            "hgnc_id": hgnc_id,
        }
        return hit


if __name__ == "__main__":
    gene_mapper = GeneNormalizer()

    # # CRIPAK, CEFIP, TOP6BL
    # input_genes = ["TP53", "KRAS", "KRAS2", "BRCA2", "FACD", "BLABLA"]
    # expected_results = [True, True, False, True, False, False]

    # for gene, result in zip(input_genes, expected_results):
    #     assert gene_mapper.is_approved(gene) == result

    # expected_results = ["TP53", "KRAS", "KRAS", "BRCA2", "BRCA2", None]

    # for gene, result in zip(input_genes, expected_results):
    #     assert gene_mapper.get_approved_symbol(gene) == result

    # print([gene_mapper.get_approved_symbol(gene) for gene in input_genes])

    print(gene_mapper.get_all_info("PUM"))
