import helper.helper as helper
import re
from pandas import read_csv

import hgvs
from hgvs.enums import Datum

from exceptions.ExceptionClasses import NoPossibleSNVExchange


class AminoToGenome:

    def __init__(self, dataprovider, assemblymapper, ncbi_dict) -> None:
        self.dataprovider = dataprovider
        self.assemblymapper = assemblymapper
        self.ncbi_dictionary = ncbi_dict

    def amino_to_genome(self, gene, variant, transcript):
        """
        Retrieves information on a protein on DNA level

        :param gene:
        :param variant:
        :param transcript:
        :return:
        """

        # Split the input with the format: X123Y into a=X -> str, b=Y -> str, c=123 -> int

        # AXIN2:S636G
        ref_aa, pos_aa, var_aa = re.findall(r"\D+|\d+", variant)

        # If three letter Code
        if len(ref_aa) == 3:
            ref_aa = helper.convert_three_letter_aa(ref_aa)[0]
        if len(var_aa) == 3:
            var_aa = helper.convert_three_letter_aa(var_aa)[0]

        pos_aa = int(pos_aa)

        cds_start = transcript["cds_start_i"]
        cds_end = transcript["cds_end_i"]
        transcript_id = transcript["tx_ac"]
        genome_id = transcript["alt_ac"]
        aln_method = transcript["alt_aln_method"]

        # Get the direction of the strand (forward: +, reverse: -)
        strand = self.dataprovider.get_tx_exons(transcript_id, genome_id, aln_method)[0][4]
        if strand == -1:
            strand = "-"
        else:
            strand = "+"

        chromosome = genome_id.split(".")[0][-2:]
        if chromosome[0] == "0":
            chromosome = f"{chromosome[-1]}"
        else:
            chromosome = f"{chromosome}"

        # Calculate the codon start position based on the coding sequence
        codon_start_pos = (pos_aa * 3) - 2

        # Calculate the codon start position based on the transcript sequence
        reference_pos_start = codon_start_pos + cds_start - 1
        reference_pos_end = codon_start_pos + cds_start + 2

        # Get the triplet for the input var based on the previously calculated coordinates
        ref_codon = self.dataprovider.get_seq(transcript_id, reference_pos_start, reference_pos_end)

        # TODO: check if the input_var + position and the calculated ref_codon is correct
        # Example: G123Y -> On position 123 is a Codon that translates into G like GGT

        # Read the possible codons for all aminoacids and lo
        aa_codons = read_csv("res/data/aa_codons.csv")
        var_codons = aa_codons.loc[aa_codons["One-Letter"] == var_aa]["Codons"].item()

        # Get the first possible nucleotide exchange with just one nucleotide involved
        replacement, position = helper.get_codon_single_exchange(ref_codon, var_codons)
        # No exchange with just one nucleotide exchange found
        if replacement == -1:
            print(f"No single Nucleotide replacements found for {ref_codon} and {var_codons}")
            raise NoPossibleSNVExchange(ref_codon, var_codons)

        replacement_from, replacement_to = replacement.split(">")

        # Find one possible exchange based on 'ref_codon' -> exchange_pos (1,2,3)
        # and the nuceleotide (A,C,G,T)
        exchange_position = position - 1
        exchange_nucleotide = replacement_to

        # Generate sequence object based on HGVS-documentation, in order to correctly convert
        # coordinates from c-dna to g-dna
        # Read more documentation in order to understand all paramaters like: datum, uncertain, etc.
        pos = hgvs.location.BaseOffsetPosition(
            base=codon_start_pos + exchange_position, offset=0, datum=Datum.CDS_START, uncertain=False
        )
        interval = hgvs.location.Interval(start=pos, end=pos, uncertain=False)
        edit = hgvs.edit.NARefAlt(ref=ref_codon[exchange_position], alt=exchange_nucleotide, uncertain=False)
        posedit = hgvs.posedit.PosEdit(pos=interval, edit=edit, uncertain=False)
        seqvar = hgvs.sequencevariant.SequenceVariant(ac=transcript_id, type="c", posedit=posedit)

        # Use hgvs to convert the coordinate from c-dna to g-dna
        result = self.assemblymapper.c_to_g(seqvar)

        # Create a json-object return all the important informations generated in this function
        return [
            {
                "transcript_id": transcript_id,
                "hgnc_symbol": gene,
                "chromosome": chromosome,
                "pos_start": int(result.posedit.pos.start.base - exchange_position),
                "pos_end": int(result.posedit.pos.start.base - exchange_position) + (len(ref_aa) * 3) - 1,
                "strand": strand,
                "refAllele": str(result.posedit.edit.ref),
                "varAllele": str(result.posedit.edit.alt),
                "refCodon": ref_codon,
                "varCodon": ref_codon[:exchange_position] + exchange_nucleotide + ref_codon[exchange_position + 1 :],
                "refAmino": ref_aa,
                "ref": ref_aa,
                "var": var_aa,
                "start": pos_aa,
                "end": pos_aa,
                "aminoacid_exchange": variant,
                "varAmino": var_aa,
                "cds_start": cds_start,
                "cds_end": cds_end,
                "prot_location": pos_aa,
                "input_var": f"{gene}:{variant}",
                "c_dna_string": str(seqvar),
                "results_string": str(result),
                "nucleotide_change": f"{ref_codon} --> {var_codons.replace(';', '|')}",
                "query_id": "0",
                "canonical": "0",
            }
        ]
