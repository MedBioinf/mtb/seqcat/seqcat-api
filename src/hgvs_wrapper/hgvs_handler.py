
# from multiprocessing import cpu_count
# from joblib import Parallel, delayed


class HGVSHandler:
    def __init__(self, input_variants, hgvs_function, kwargs={}) -> None:
        self.input_variants = input_variants.split(",")
        self.hgvs_function = hgvs_function
        self.kwargs = kwargs
        # self.cpu_count = cpu_count()

    @staticmethod
    def serviceOutput(service_result, qid, status_code=200, error_message=None) -> dict:
        output_dict = {}
        output_dict['data'] = service_result
        output_dict['header'] = {
            'qid': qid,
            'status_code': status_code,
            'error_message': error_message
        }

        return_dict = {
            'header': output_dict['header'],
            'data': output_dict['data']
        }

        return return_dict

    def batchProcessing(self) -> list:
        # results = Parallel(n_jobs=self.cpu_count-1, backend='threading')(delayed(self.annotateInputVariants)(element) for element in self.input_variants)
        results = [self.annotateInputVariants(element) for element in self.input_variants]
        return results

    def annotateInputVariants(self, element) -> dict:
        element = element.strip()
        try:
            service_result = self.hgvs_function(element, **self.kwargs)
            service_dict = self.serviceOutput(service_result, element)
        except Exception as exc:
            # print(str(exc))
            service_dict = self.serviceOutput(None, element, status_code=400, error_message=str(exc))
        return service_dict
