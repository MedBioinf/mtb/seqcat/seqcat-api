class FusionInFrame:
    def fusion_is_inframe(self, input_1, genomic_pos_1, transcript_info_1, input_2, genomic_pos_2, transcript_info_2):
        """
        TODO: docstring
        """
        return_value_1 = self._position_is_inframe(genomic_pos_1, transcript_info_1)
        return_value_1["qid"] = input_1
        return_value_2 = self._position_is_inframe(genomic_pos_2, transcript_info_2)
        return_value_2["qid"] = input_2

        triplett_position_1 = return_value_1["position_in_triplett"]
        triplett_position_2 = return_value_2["position_in_triplett"]

        if (triplett_position_1 + triplett_position_2) % 3 == 0:
            in_frame = "In-Frame"
        else:
            in_frame = "Out_of_Frame"

        result = {"input_1": return_value_1, "input_2": return_value_2, "is_inframe": in_frame}

        return result

    def _position_is_inframe(self, genomic_pos, transcript_info):
        genomic_pos_res = genomic_pos

        # check if location is intron
        is_intron = self._fusion_pos_is_in_intron(genomic_pos_res, transcript_info)

        if is_intron:
            return "WARNING: No Exon position found. Probably Intron."

        try:
            is_inframe_dict = self._fusion_pos_is_in_frame(genomic_pos_res, transcript_info)
        except NameError as e:
            raise e

        return is_inframe_dict

    def _fusion_pos_is_in_intron(self, genomic_pos, transcript_info):
        genomic_exon_positions = [
            (exon["chromosome_start_i"], exon["chromosome_end_i"]) for exon in transcript_info["exons"]
        ]

        for genomic_exon_position in genomic_exon_positions:
            if (genomic_pos >= genomic_exon_position[0]) & (genomic_pos <= genomic_exon_position[1]):
                return False
        return True

    def _fusion_pos_is_in_frame(self, genomic_pos, transcript_info):
        cds_start = transcript_info["cds_start_i"]
        cds_end = transcript_info["cds_end_i"]
        strand = int(transcript_info["strand"])

        if strand == -1:
            transcript_exon_positions = [(exon["tx_start_i"], exon["tx_end_i"]) for exon in transcript_info["exons"]]

            genomic_exon_positions = [
                (exon["chromosome_start_i"], exon["chromosome_end_i"]) for exon in transcript_info["exons"]
            ]

            start_exon, genomic_cds_start = self._get_cds_start_exon_transcript(
                cds_start, transcript_info, reverse_strand=True
            )
            target_exon, summed_length = self._get_target_exon_genomic(
                genomic_pos, genomic_cds_start, start_exon, transcript_info, reverse_strand=True
            )

            if type(summed_length) == str:
                return summed_length

            final_position = summed_length
            final_position_mod = 2 - ((final_position + 1) % 3)

        else:
            transcript_exon_positions = [(exon["tx_start_i"], exon["tx_end_i"]) for exon in transcript_info["exons"]]

            genomic_exon_positions = [
                (exon["chromosome_start_i"], exon["chromosome_end_i"]) for exon in transcript_info["exons"]
            ]

            start_exon, genomic_cds_start = self._get_cds_start_exon_transcript(
                cds_start, transcript_info, reverse_strand=False
            )
            target_exon, summed_length = self._get_target_exon_genomic(
                genomic_pos, genomic_cds_start, start_exon, transcript_info, reverse_strand=False
            )

            if type(summed_length) == str:
                return summed_length

            final_position = summed_length
            final_position_mod = final_position % 3

        if "Exon" in str(final_position):
            raise NameError(final_position)

        return_dict = {
            "gene_name": transcript_info["hgnc"],
            "transcript": transcript_info["transcript"],
            "strand": "+" if transcript_info["strand"] == 1 else "-",
            "target_exon": target_exon + 1,
            "breakpoint_position_in_transcript": final_position,
            "position_in_triplett": final_position_mod,
            "position_in_frame": True if final_position_mod == 0 else False,
        }

        return return_dict

    def _get_cds_start_exon_transcript(self, transcript_pos, transcript_info, reverse_strand=False):
        ## Function returns the cds start exon and the position
        sequence_length = 0

        if reverse_strand:
            transcript_iterator = reversed(list(enumerate(transcript_info["exons"])))
            exon_length_list = list(reversed(transcript_info["exon_lengths"]))
        else:
            transcript_iterator = enumerate(transcript_info["exons"])
            exon_length_list = transcript_info["exon_lengths"]

        for i, exon in transcript_iterator:
            start = exon["tx_start_i"]
            end = exon["tx_end_i"]
            exon_length = exon_length_list[i]

            if transcript_pos > start and transcript_pos < end:
                if not reverse_strand:
                    genomic_shift = transcript_pos + sequence_length + 1
                    genomic_exon_position = exon["chromosome_start_i"] + genomic_shift
                else:
                    genomic_shift = transcript_pos - sequence_length
                    genomic_exon_position = exon["chromosome_end_i"] - genomic_shift
                return i, genomic_exon_position
            else:
                sequence_length += exon_length

        raise Exception("WARNING")

    def _get_target_exon_genomic(
        self, genomic_pos, genomic_cds_start, exon_start, transcript_info, reverse_strand=False
    ):
        sequence_length = 0

        # If reversed strand, then reverse the exon list and the length list
        if reverse_strand:
            transcript_iterator = enumerate(reversed(list(transcript_info["exons"])))
        else:
            transcript_iterator = enumerate(transcript_info["exons"])
        
        # This list is already sorted from exon 0 to n, no matter the strand
        exon_length_list = transcript_info["exon_lengths"]

        for i, exon in transcript_iterator:
            start = exon["chromosome_start_i"]
            end = exon["chromosome_end_i"]
            exon_length = exon_length_list[i]

            if not reverse_strand:
                if (genomic_pos >= start) and (genomic_pos <= end):
                    shift = genomic_pos - exon["chromosome_start_i"]
                    sequence_length += shift + 1
                    return i, sequence_length
                else:
                    if i == exon_start:
                        sequence_length += exon["chromosome_end_i"] - genomic_cds_start
                    if i < exon_start:
                        sequence_length += exon_length
            else:
                if (genomic_pos >= start) and (genomic_pos <= end):
                    shift = exon["chromosome_end_i"] - genomic_pos
                    sequence_length += shift
                    return i, sequence_length
                else:
                    if i == exon_start:
                        sequence_length += genomic_cds_start - exon["chromosome_start_i"]
                    if i < exon_start:
                        sequence_length += exon_length

        raise Exception("WARNING: No Exon position found. Probably Intron.")
