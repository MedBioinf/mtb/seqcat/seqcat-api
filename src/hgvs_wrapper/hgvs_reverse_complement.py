"""
This module contains the ReverseComplement class, which is used to get the reverse complement of a sequence.
"""

class ReverseComplement:
    """
    Class for getting the reverse complement of a sequence. If the sequence contains any characters other than A, T, C,
    G, a, t, c, g, n, N, they are replaced with "?".
    """
    sequence: str
    mappings = {"A": "T", "T": "A", "C": "G", "G": "C", "a": "t", "t": "a", "c": "g", "g": "c", "n": "n", "N": "N"}
    results: str

    def __init__(self, sequence: str, conversion_type='reverse_complement') -> None:
        self.sequence = sequence
        self.conversion_type = conversion_type

        if self.conversion_type == 'reverse_complement':
            self.results = self.get_reverse_complement()
        elif self.conversion_type == 'reverse':
            self.results = self.get_reverse()
        elif self.conversion_type == 'complement':
            self.results = self.get_complement()
        

    def get_reverse_complement(self) -> str:
        """
        Returns the reverse complement of the sequence. If the sequence contains any characters other than A, T, C, G,
        a, t, c, g, n, N, they are replaced with "?".
        :return: reverse complement of the sequence
        """
        return "".join([self.mappings.get(base, "?") for base in reversed(self.sequence)])

    
    def get_reverse(self) -> str:
        """
        Returns the reverse of the sequence. 
        :return: reverse of the sequence
        """
        return "".join([base for base in reversed(self.sequence)])
    
    def get_complement(self) -> str:
        """
        Returns the complement of the sequence. If the sequence contains any characters other than A, T, C, G,
        a, t, c, g, n, N, they are replaced with "?".
        :return: complement of the sequence
        """
        return "".join([self.mappings.get(base, "?") for base in self.sequence])


if __name__ == "__main__":
    sequence = "ATCGabvfsASDVA123NNNnnnn412"
    rc = ReverseComplement(sequence, 'complement')
    print(rc.results)
