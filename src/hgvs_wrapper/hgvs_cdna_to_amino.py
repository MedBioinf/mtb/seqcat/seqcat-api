import re
from pandas import read_csv
import exceptions.ExceptionClasses as ExceptionClasses
from exceptions.ExceptionClasses import NoTranscriptFoundException


class CDNAtoAmino:

    def __init__(self, dataprovider, assemblymapper, parser, validator, ncbi_dict) -> None:
        self.dataprovider = dataprovider
        self.assemblymapper = assemblymapper
        self.parser = parser
        self.validator = validator
        self.ncbi_dictionary = ncbi_dict

    def cdna_to_amino(self,
                      gene: str,
                      input_var: str,
                      get_transcripts) -> dict:
        """
        Converts a cDNA identifier to a protein

        :param gene: HUGO gene symbol or RefSeq transcript identifier
        :param input_var: cDNA identifier
        :param get_transcripts:
        :return:
        """

        transcript = None

        try:
            if gene.startswith("NM_"):
                transcript = self.dataprovider.get_tx_identity_info(gene)['tx_ac']
                input_orig = input_var
                input_var = f'{transcript}:{input_var}'
                gene = self.dataprovider.get_tx_identity_info(gene)[-1]
            else:
                transcripts = get_transcripts(gene)
                transcript = transcripts["ncbi_mane_transcript"]['tx_ac']
                input_orig = input_var
                if ">" not in input_var:
                    input_regex = re.search(r"([A-Za-z]*)(\d*)([A-Za-z]*)$", input_var)
                    input_var = f'{transcript}:c.{input_regex.group(2)}{input_regex.group(1)}>{input_regex.group(3)}'
                else:
                    input_var = f'{transcript}:{input_var}'
        except NoTranscriptFoundException as e:
            return str(e)

        if 'c.' not in input_var:
            input_var = input_var.split(":")[0] + ":c." + input_var.split(":")[1]

        parsed = self.parser.parse_hgvs_variant(input_var)

        try:
            valid = self.validator.validate(parsed)
        except Exception as exc:
            raise ExceptionClasses.WrongReferenceGenomeException(exc)

        var_p = self.assemblymapper.c_to_p(parsed)
        var_g = self.assemblymapper.c_to_g(parsed)

        print(var_g)

        aa_exchange_three = str(var_p.posedit).strip('()')
        aa_exchange_three_regex = re.findall(r"\D+|\d+", aa_exchange_three)

        ref_aa_three = aa_exchange_three_regex[0]
        pos = aa_exchange_three_regex [1]
        var_aa_three = aa_exchange_three_regex[2]

        aa_codons = read_csv('res/data/aa_codons.csv')

        if ">" in input_var:
            ref_aa = aa_codons.loc[aa_codons["Three-Letter"] == ref_aa_three]["One-Letter"].item()

            var_aa = "=" if var_aa_three == "=" else aa_codons.loc[aa_codons["Three-Letter"] == var_aa_three]["One-Letter"].item()
            variant_exchange = ""
            variant_exchange_long = ""
        else:
            variant_exchange = None
            variant_exchange_long = None

        results = {
                    'parsed_data': str(input_var),
                    'validation': valid,
                    'transcript': transcript,
                    'gene_name': gene,
                    'chromosome': self.ncbi_dictionary[var_g.ac.lower()],
                    'variant_g': str(var_g),
                    'variant_p': str(var_p)
                   }
        if variant_exchange is not None:
            results['variant_exchange_long'] = aa_exchange_three
            results['variant_exchange'] = ref_aa + pos + var_aa

        # TODO return for each transcript the possible exchanges

        return results
