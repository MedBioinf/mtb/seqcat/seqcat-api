from .multiprocessing_symbols_extraction import extract_symbols
from .Build_Adjacency_Matrix import build_adjacency_matrix
from .Gravis_html_generator import generate_gravis_html
import requests
import os
from Bio.KEGG.KGML.KGML_parser import read
import numpy as np


def generate_pathway_html(pathway_name, target_genes, target_relations, target_generations):
    print("program started")
    res_path = "res/data/pathways_resources"
    if not os.path.exists(res_path):
        os.makedirs(res_path)

    def search_pathway(pathway_name):
        print("searching pathway")
        url = f"https://rest.kegg.jp/get/{pathway_name}/kgml"
        """
        eg. url = "https://rest.kegg.jp/get/hsa05200/kgml"
        """
        try:
            if not os.path.exists(f"{res_path}/KGMLs_Matrices"):
                os.makedirs(f"{res_path}/KGMLs_Matrices")

            if pathway_name + ".xml" in os.listdir(f"{res_path}/KGMLs_Matrices"):
                pass

            else:
                r = requests.get(url)
                with open(f"{res_path}/KGMLs_Matrices/{pathway_name}.xml", "wb") as f:
                    f.write(r.content)
                    print("pathway downloaded")
        except:
            pass

        file_path = "KGMLs_Matrices/" + pathway_name + ".xml"

        return file_path

    pathway_file_name = search_pathway(pathway_name)
    print("extracting symbols")
    symbols_path = extract_symbols(pathway_file_name, res_path=res_path)
    print("building adjacency matrix")
    matrix_path = build_adjacency_matrix(pathway_file_name, symbols_path=symbols_path, res_path=res_path)
    print("generating html and image")
    html = generate_gravis_html(
        matrix_path,
        symbols_path=symbols_path,
        target_genes=target_genes,
        target_relations=target_relations,
        target_generations=target_generations,
        res_path=res_path,
    )
    return html


def early_process(pathway_name):
    res_path = "../../../res/data/pathways_resources"

    def search_pathway(pathway_name):
        print("searching pathway")
        url = f"https://rest.kegg.jp/get/{pathway_name}/kgml"
        """
        eg. url = "https://rest.kegg.jp/get/hsa05200/kgml"
        """
        try:
            if not os.path.exists(f"{res_path}/KGMLs_Matrices"):
                os.makedirs(f"{res_path}/KGMLs_Matrices")

            if pathway_name + ".xml" in os.listdir(f"{res_path}/KGMLs_Matrices"):
                pass
                print(f"{pathway_name}pathway already downloaded and saved in {res_path}/KGMLs_Matrices")

            else:
                r = requests.get(url)
                with open(f"{res_path}/KGMLs_Matrices/{pathway_name}.xml", "wb") as f:
                    f.write(r.content)
                    print("pathway downloaded")
        except:
            pass

        file_path = "KGMLs_Matrices/" + pathway_name + ".xml"

        return file_path

    def load_targets(pathway_name):
        pathway = read(open(pathway_name, "r"))
        targets = []
        for entry_id in pathway.entries:
            li = pathway.entries[entry_id].name.split(" ")
            for item in li:
                if item != "undefined":
                    targets.extend(li)
        targets = np.unique(targets)
        return targets

    pathway_file_name = search_pathway(pathway_name)
    pathway_file = f"{res_path}/{pathway_file_name}"
    print(f"loading targets from {pathway_file}")
    targets = load_targets(pathway_file)
    print(type(targets))
    print(f"targets: {targets}")

    return targets
