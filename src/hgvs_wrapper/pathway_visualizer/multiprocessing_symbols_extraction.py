import concurrent
import json
import os
from concurrent.futures import ThreadPoolExecutor

import numpy as np
import requests
import multiprocessing
from joblib import Parallel, delayed
import re
from Bio.KEGG.KGML.KGML_parser import read
import tqdm


def extract_symbols(pathway_file_name, res_path):
    '''
    This function is used to extract the symbols from the KEGG website. The symbols are used to replace the gene IDs
    in the pathway. The symbols are stored in a json file to avoid extracting them again.
    param pathway_file_name: the name of the pathway file
    :return: the path to the symbols json file
    '''

    def load_previous_symbols():
        if not os.path.exists(f'{res_path}/symbols.json'):
            with open(f'{res_path}/symbols.json', 'w') as fp:
                json.dump({"cpd:C01245":"D-myo-Inositol 1"}, fp)
        with open(f'{res_path}/symbols.json', 'r') as fp:
            symbols_dict = json.load(fp)
        return symbols_dict

    #prev_symbols = load_previous_symbols()
    final_dict = load_previous_symbols()

    def load_pathway(pathway_name):
        pathway = read(open(pathway_name, 'r'))
        return pathway

    pathway = load_pathway(f'{res_path}/{pathway_file_name}')


    def extract_symbol(link, indx=0):
        # extracting the sybmol from the link attribute of the gene
        doc = requests.get(link).text
        try: # gene symbol
            # everything between <td class="td11 defd"><div class="cel"> and <br> </div></td>
            pattern = re.compile(r'<td class="td11 defd"><div class="cel">(.*)<')
            # find all the matches
            symbols = pattern.findall(doc)[0]
            # choosing the symbol by index (since there could be multiple symbols in one link)
            symbol = symbols.split(", ")[indx]

        except:
            try: # compound symbol
                pattern = re.compile(r'<div class="cel">(.*);<br>')
                symbol = pattern.findall(doc)[0]
                if symbol.split(">")[0]:
                    symbol = symbol.split(">")[1]
            except:
                pass
                # return the input
                symbol = link.split(":")[-1]
        return symbol


    def dig(gene, final_dict):

        try:
            ids = gene.name.split(" ") # returns the IDs eg. hsa:7157, hsa:7158
            for indx, g in enumerate(ids):
                if g not in final_dict.keys():
                    link = f"https://www.kegg.jp/dbget-bin/www_bget?{g}"
                    #print("working on link: ", link)
                    final_dict[g] = extract_symbol(link)

        except:
            pass

        return final_dict


    #final_dict = {}


    itemss = []
    for entry_id in pathway.entries:
        if pathway.entries[entry_id].name not in final_dict.keys():
            if pathway.entries[entry_id].name != "undefined":
                itemss.append(pathway.entries[entry_id])

    for relation in pathway.relations:
        if relation.entry1.name not in final_dict.keys():
            itemss.append(relation.entry1)
        if relation.entry2.name not in final_dict.keys():
            itemss.append(relation.entry2)

    count = 0
    items_copy = itemss.copy()
    for itm in items_copy:
        keys = itm.name.split(" ")
        itemss.remove(itm)
        for key in keys:
            if key in final_dict.keys():
                count += 1
            else:
                itemss.append(itm)

    if len(itemss) != 0:
        itemss = list(set(itemss)) # remove duplicates

        print(count, "symbols already extracted")
        print("extracting symbols for: ", len(itemss), "new items", "eg. ", itemss[0].name)

        # applying multiprocessing to speed up the process
        number_of_cores = int(multiprocessing.cpu_count()/2) # TODO must be reduced according to available resources

        def process_item(itm):
            return dig(itm, final_dict)

        with tqdm.tqdm(total=len(itemss)) as pbar:
            def update_pbar(*args, **kwargs):
                pbar.update()

            with ThreadPoolExecutor(max_workers=number_of_cores) as executor:
                futures = [executor.submit(process_item, itm) for itm in itemss]

                for future in concurrent.futures.as_completed(futures):
                    # Update progress bar after each completed task
                    update_pbar()

                    # Get the result from the completed task
                    output = future.result()

        #for _ in tqdm.tqdm(itemss):
        #    output = dig(_, final_dict)

        final_dict.update(output)

        with open(f'{res_path}/symbols.json', 'w') as fp:
            json.dump(final_dict, fp)

    symbols_path = f'{res_path}/symbols.json'

    return symbols_path

