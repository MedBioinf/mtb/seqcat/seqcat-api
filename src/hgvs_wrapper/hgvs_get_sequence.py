

class GetSequence():
    def __init__(self, seqfetcher, ncbi_dict) -> None:
        self.seqfetcher = seqfetcher
        self.ncbi_dictionary = ncbi_dict


    # input var in format chr1:1000-2000
    def get_sequence(self, input_var):
        input_chrom = input_var.split(":")[0].lower()
        input_chrom_number = input_chrom.strip("chr")
        parsed_chrom = self.ncbi_dictionary[input_chrom]

        pos_range = input_var.split(":")[1].lower()
        start = int(pos_range.split("-")[0])
        end = int(pos_range.split("-")[1])

        if start > end:
            raise(Exception("Start position can not be greater that stop position."))

        sequence = self.seqfetcher.fetch_seq(parsed_chrom, start, end)

        return_dict = {
            "chromosome": parsed_chrom,
            "start": start,
            "end": end,
            "sequence": sequence
        }

        return return_dict