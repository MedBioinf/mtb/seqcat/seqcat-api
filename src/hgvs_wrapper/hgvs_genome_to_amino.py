import re
import adagenes
import exceptions.ExceptionClasses as ExceptionClasses


class GenomeToAmino:
    def __init__(self, dataprovider, assemblymapper, parser, validator, ncbi_dict, gene_positions) -> None:
        self.dataprovider = dataprovider
        self.assemblymapper = assemblymapper
        self.parser = parser
        self.validator = validator
        self.ncbi_dictionary = ncbi_dict
        self.gene_positions = gene_positions

    def genome_to_amino(self, input_var, get_transcripts, aa_codons):
        """
        TODO: Module docstring
        """

        # Parse and create HGVS usable input
        # String and dict operations O(1)
        # Regex operation should be not expensive problem at this string size



        # Input Var: 'chr1:942136G>T'
        input_chrom = input_var.split(":")[0].lower()
        input_chrom_number = input_chrom.strip("chr")
        parsed_chrom = self.ncbi_dictionary[input_chrom]
        input_var_parsed = parsed_chrom + ":" + input_var.split(":")[1]
        input_var_exchange = re.search(r"[a-zA-z]+>[a-zA-Z]+", input_var_parsed)

        if input_var_exchange is not None:
            input_var_pos = re.search(r"\d+", input_var.split(":")[1]).group(0)
            input_var_start = input_var_pos
            input_var_end = input_var_pos
        else:
            request_type, groups = adagenes.get_variant_request_type(input_var)
            if (request_type == "deletion") or (request_type == "deletion_nc") \
                    or (request_type == "insertion") or (request_type == "insertion_nc") \
                    or (request_type == "indel") or (request_type == "indel_nc"):
                input_var_start = groups[3]
                input_var_end = groups[3]
            elif (request_type == "deletion_long") or (request_type == "deletion_nc_long") \
                    or (request_type == "insertion_long") or (request_type == "insertion_nc_long") \
                    or (request_type == "indel_long") or (request_type == "indel_nc_long"):
                print(groups)
                input_var_start = groups[3]
                input_var_end = groups[4]

        # No regex-obj found -> Variant exchange is not supported for HGVS
        #if not input_var_exchange:
        #    raise ExceptionClasses.BadInputException

        # Check if variant exchange is exactly 1 nucleotide to 1 nucleotide
        # String operations O(1)
        if input_var_exchange is not None:
            input_var_exchange = input_var_exchange[0].split(">", 1)
        #if len(input_var_exchange[0]) != 1 or len(input_var_exchange[1]) != 1:
        #    raise ExceptionClasses.BadInputException

        # String operations O(1)
        if "g." not in input_var_parsed:
            input_var_parsed = input_var_parsed.split(":")[0] + ":g." + input_var_parsed.split(":")[1]

        # First HGVS call
        # Profiler tells 0.01% running time
        parsed = self.parser.parse_hgvs_variant(input_var_parsed)
        varg = self.parser.parse(input_var_parsed)
        print("parsed ",parsed)
        print("varg ",varg)

        # Profiler tells that validator uses 0.11% running time
        try:
            valid = self.validator.validate(parsed)
        except Exception as exc:
            raise ExceptionClasses.WrongReferenceGenomeException(exc)

        # Assemblymapper uses 3.41% running time
        # transcripts = self.assemblymapper.relevant_transcripts(parsed)

        # gene_name = self.dataprovider.get_tx_identity_info(transcripts[0])[-1]
        print(self.gene_positions)

        if input_chrom_number == 'x' or input_chrom_number == 'y':
            input_chrom_number = input_chrom_number.upper()

        print("chrom ",input_chrom_number," start: ",input_var_start," end: ",input_var_end)
        genes = self.gene_positions[
            (self.gene_positions["Chromosome"] == input_chrom_number)
            & (self.gene_positions["Start"] <= int(input_var_start))
            & (self.gene_positions["Stop"] >= int(input_var_end))
        ]["Gene"]
        gene_name = None
        if len(genes.values) > 0:
            gene_name = genes.values[0]
            print(gene_name)
        else:
            print("No gene found: ",input_var)

        if gene_name is None:
            raise Exception("No gene name found at that position." + str(input_var))

        # gene_name = 'SAMD11'

        # Get transcripts from local files
        # Get transcripts uses 0.27% running time
        transcript_ncbi = get_transcripts(gene_name)["ncbi_mane_transcript"]['tx_ac']
        print("transcripts ",transcript_ncbi)

        # Check if NCBI transcript is in transcripts
        try:
            var_t = self.assemblymapper.g_to_t(parsed, transcript_ncbi)
            print("vart ",var_t)
            var_p = self.assemblymapper.t_to_p(var_t)
        except Exception as exc:
            print(exc)
            raise ExceptionClasses.NoTranscriptFoundException(gene_name)

        if "Met1?" not in str(var_p) and "?" in str(var_p):
            raise ExceptionClasses.IntronAreaException

        # tx_info = self.dataprovider.get_tx_identity_info(transcript_ncbi)

        aa_exchange_three = str(var_p.posedit).strip("()")
        aa_exchange_three_regex = re.findall(r"\D+|\d+", aa_exchange_three)

        ref_aa_three = aa_exchange_three_regex[0]
        pos = aa_exchange_three_regex[1]
        var_aa_three = aa_exchange_three_regex[2]

        ref_aa = aa_codons.loc[aa_codons["Three-Letter"] == ref_aa_three]["One-Letter"].item()
        var_aa = (
            "="
            if var_aa_three == "="
            else aa_codons.loc[aa_codons["Three-Letter"] == var_aa_three]["One-Letter"].item()
        )

        results = {
            "input_data": input_var,
            "parsed_data": str(input_var_parsed),
            "validation": valid,
            "transcript": transcript_ncbi,
            #'transcript_info': tx_info,
            "gene_name": gene_name,
            #'gene_name_transcripts': gene_transcripts,
            "variant": str(var_p),
        }
        if aa_exchange_three is not None:
            results["variant_exchange_long"] = aa_exchange_three
            results["variant_exchange"] = ref_aa + pos + var_aa

        # TODO return for each transcript the possible exchanges

        return results


class GenomeSequenceToAmino:
    def __init__(self, dna_sequence, aa_codons) -> None:
        self.dna_sequence = dna_sequence
        self.aa_codons = aa_codons
        self.info = {"warning": []}
        self.aa_sequence = self.convert_sequence()

    def __str__(self) -> str:
        return str(self.aa_sequence)

    def convert_sequence(self):
        dna_sequence = self.dna_sequence.upper()

        # split sequence in pairs of 3
        dna_sequence_codons = [dna_sequence[i : i + 3] for i in range(0, len(dna_sequence), 3)]

        # check if last codon is complete and remove if not
        if len(dna_sequence_codons[-1]) != 3:
            self.info["warning"].append(f"The last codon is not complete ({dna_sequence_codons[-1]}) is replaced by '?'")
            dna_sequence_codons[-1] = "?"

        # check if all codons are valid if not replace it with ?
        for i, codon in enumerate(dna_sequence_codons):
            if re.search(r"[^ATCG\?]", codon):
                self.info["warning"].append(f"The codon at position {i} contains a character unlike [A,C,G,T] -> ({codon}) is replaced by '?'")
                dna_sequence_codons[i] = "?"
                

        one_letter_seq = [
            self.aa_codons.loc[self.aa_codons["Codons"].str.contains(x, regex=False)]["One-Letter"].item() for x in dna_sequence_codons
        ]
        three_letter_seq = [
            self.aa_codons.loc[self.aa_codons["Codons"].str.contains(x, regex=False)]["Three-Letter"].item() for x in dna_sequence_codons
        ]

        return {"dna_sequence": dna_sequence, "one_letter_seq": "".join(one_letter_seq), "three_letter_seq": "".join(three_letter_seq)}

    def get_results(self):
        return_dict = {
            "aa_sequence": self.aa_sequence,
            "info": self.info,
        }
        return return_dict
