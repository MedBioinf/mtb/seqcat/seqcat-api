"""
This package handles the transcript information for a gene name or a transcript accession
"""
from typing import List, Dict, Tuple

import pandas as pd
from pandas import read_csv
from exceptions.ExceptionClasses import NoTranscriptFoundException

MANE_FILEPATH = "res/data/MANE_GRCh38_v1_0_summary.txt"
MANE_GENE_COL = "symbol"
MANE_NCBI_COL = "RefSeq_nuc"
MANE_EMBL_COL = "Ensembl_nuc"
MANE_TX_COL = "TX_ID"
MANE_SELECT_STATUS = "MANE Select"
EMBL_DB = "Ensembl"
NCBI_DB = "NCBI"


class TranscriptHandler:
    def __init__(self, dataprovider) -> None:
        self.dataprovider = dataprovider

        self.mane_df = read_csv(MANE_FILEPATH, sep="\t")

        self.embl_txs = self.mane_df.loc[
            self.mane_df.MANE_status == MANE_SELECT_STATUS, [MANE_GENE_COL, MANE_EMBL_COL]
        ].copy()
        self.embl_txs.columns = [MANE_GENE_COL, MANE_TX_COL]

        self.ncbi_txs = self.mane_df.loc[
            self.mane_df.MANE_status == MANE_SELECT_STATUS, [MANE_GENE_COL, MANE_NCBI_COL]
        ].copy()
        self.ncbi_txs.columns = [MANE_GENE_COL, MANE_TX_COL]

    def get_transcripts(self, gene_name: str) -> List[Dict[str, str]]:
        """
        Returns all transcripts for a gene name
        :param gene_name: Gene name
        :return: List of ordered dictionarys for transcripts with the following attributes:\n
            \t'hgnc': Gene name
            \t'cds_start_i': CDS start index
            \t'cds_end_i': CDS end index
            \t'tx_ac': Transcript accession
            \t'alt_ac': Genome accession
            \t'alt_strand': Genome Strand
            \t'alt_aln_method': Alignment method
        :raises NoTranscriptFoundException: If no transcript is found for the gene name
        """
        txs = self.dataprovider.get_tx_for_gene(gene_name)
        if txs:
            return txs
        raise NoTranscriptFoundException(gene_name)

    def get_structured_transcripts(self, gene_name: str) -> Dict[str, Dict[str, str]]:
        """
        Returns all transcripts for a gene name sorted into NCBI and ENSEMBL transcripts and the assoziated MANE transcripts
        :param gene_name: Gene name
        :return: Dictionary with the following keys:\n
            \t'ncbi_transcripts': list of all available ncbi_tx,
            \t'ensmbl_transcripts': list of all available ensembl_tx,
            \t'ncbi_mane_transcript': ncbi_mane_transcript,
            \t'ensmbl_mane_transcript': ensmbl_mane_transcript
        """
        transcripts = self.get_transcripts(gene_name)
        ncbi_tx, ensembl_tx = self._sort_transcripts(transcripts)
        ncbi_mane_transcript = self.get_mane_transcripts(gene_name, ncbi_tx, NCBI_DB)
        ensmbl_mane_transcript = self.get_mane_transcripts(gene_name, ensembl_tx, EMBL_DB)

        return_dict = {
            "ncbi_transcripts": ncbi_tx,
            "ensmbl_transcripts": ensembl_tx,
            "ncbi_mane_transcript": ncbi_mane_transcript,
            "ensmbl_mane_transcript": ensmbl_mane_transcript,
        }

        return return_dict

    def _sort_transcripts(self, tx_list: list) -> Tuple[List, List]:
        """
        Sorts a list of transcripts into NCBI and ENSEMBL transcripts
        :param tx_list: List of transcripts with tx-dictionaries
        :return: Two lists of transcripts, one for NCBI and one for ENSEMBL transcripts containing the tx-dictionaries
        """
        ncbi_list = []
        ensembl_list = []

        for tx in tx_list:
            if "NM_" in tx["tx_ac"]:
                ncbi_list.append(tx)
            elif "ENST" in tx["tx_ac"]:
                ensembl_list.append(tx)

        return ncbi_list, ensembl_list

    def get_mane_transcripts(self, gene_name: str, tx_list: list, db_string: str) -> List:
        """
        Returns the MANE transcript for a gene name and a list of transcripts
        :param gene_name: Gene name
        :param tx_list: List of transcripts with tx-dictionaries
        :param db_string: String to identify the database (NCBI or ENSEMBL)
        :return: MANE transcript as tx-dictionary or empty list if no MANE transcript is found
        """
        if db_string == NCBI_DB:
            mane_df = self.ncbi_txs
        else:
            mane_df = self.embl_txs
        mane_transcripts = self._get_mane_tx_infos(gene_name, mane_df)

        for tx in tx_list:
            if tx["tx_ac"] in mane_transcripts:
                return tx
        return []

    def _get_mane_tx_infos(self, gene_name: str, dataframe: pd.DataFrame) -> List[Dict[str, str]]:
        """
        Loads all MANE transcripts for a gene name from a dataframe
        :param gene_name: Gene name
        :param dataframe: Dataframe containing the MANE transcripts either from NCBI or ENSEMBL
        :return: List of MANE transcripts
        """
        tx_series = dataframe.loc[dataframe[MANE_GENE_COL] == gene_name, MANE_TX_COL]
        if not tx_series.empty:
            return tx_series.item()
        return None

    def get_gene_name_by_transcript(self, transcript: str) -> str:
        """
        Returns the gene name for a transcript
        :param transcript: Transcript accession
        :return: Gene name
        """
        return self.dataprovider.get_tx_identity_info(transcript)[-1]

    def get_transcript_info(self, tx: str) -> Dict[str, str]:
        """
        Returns the transcript information for a transcript
        :param tx: Transcript accession
        :return: Transcript information as dictionary with the following attributes:\n
            \t'hgnc': Gene name
            \t'cds_start_i': CDS start index
            \t'cds_end_i': CDS end index
            \t'tx_ac': Transcript accession
            \t'alt_ac': Genome accession
            \t'alt_aln_method': Alignment method
            \t'lengths': Transcript lengths of exons and introns?
        """
        return self.dataprovider.get_tx_identity_info(tx)

    def get_transcript_exons(self, tx_obj: list) -> List[List]:
        """
        Returns the transcript exons for a transcript
        :param tx_obj: Transcript object as list with the following attributes:\n
            \t'hgnc': Gene name
            \t'cds_start_i': CDS start index
            \t'cds_end_i': CDS end index
            \t'tx_ac': Transcript accession
            \t'alt_ac': Genome accession
            \t'alt_aln_method': Alignment method
        :return: List of transcript exons as list of lists with the following attributes:\n
            \t'tes_exon_set_id'
            \t'aes_exon_set_id'
            \t'tx_ac': Transcript accession
            \t'alt_ac': Genome accession
            \t'alt_strand': Genome Strand
            \t'alt_aln_method': Alignment method
            \t'ord': Order of the exon
            \t'tx_exon_id'
            \t'alt_exon_id'
            \t'tx_start_i': Exon start index in transcript
            \t'tx_end_i': Exon end index in trascript
            \t'alt_start_i': Exon start index in genome
            \t'alt_end_i': Exon start index in genome
            \t'cigar':
        """
        return self.dataprovider.get_tx_exons(tx_obj["tx_ac"], tx_obj["alt_ac"], tx_obj["alt_aln_method"])

    def get_all_transcript_info_by_gene_name(self, gene_name: str) -> Dict[str, Dict[str, str]]:
        """
        Returns all transcript information for a gene name, currently only NCBI transcripts supported
        :param gene_name: Gene name
        :return: Returns a gene-info dictionary with the following keys:\n
            \t'tx_ncbi_info': Transcript information for NCBI transcript,
            \t'tx_ncbi_exons': Transcript exons for NCBI transcript,
            \t'ncbi_transcript': NCBI transcript
        """
        tx_list_for_gene = self.get_structured_transcripts(gene_name)

        ncbi_transcript = tx_list_for_gene["ncbi_mane_transcript"]
        ncbi_transcript_id = ncbi_transcript["tx_ac"]
        # ensmbl_transcript = tx_list_for_gene["ensmbl_mane_transcript"] # currently not used

        tx_ncbi_info = self.get_transcript_info(ncbi_transcript_id)
        tx_ncbi_exons = self.get_transcript_exons(ncbi_transcript)

        return_dict = {"tx_ncbi_info": tx_ncbi_info, "tx_ncbi_exons": tx_ncbi_exons, "ncbi_transcript": ncbi_transcript}

        return return_dict

    def get_exon_sequences(self, exon_list, tx_info, seq):
        filled_exon_list = []
        return_dict = {}

        cds_start = tx_info["cds_start_i"]
        cds_end = tx_info["cds_end_i"]
        gene = tx_info["hgnc"]
        tx = tx_info["tx_ac"]

        cummulative_length = 0

        for exon in exon_list:
            exon_start = exon["tx_start_i"] if exon["tx_start_i"] != 0 else cds_start
            exon_end = exon["tx_end_i"] if exon["tx_end_i"] < cds_end else cds_end

            exon_start = exon_start - cds_start
            exon_end = exon_end - cds_start

            length = exon_end - exon_start
            cummulative_length += length

            g_seq = seq["dna_sequence"][exon_start:exon_end]

            exon_dict = {
                "exon": exon["ord"],
                "g_seq": g_seq,
                "cds_length": length,
                "cummulative_length": cummulative_length,
                "cds_start": cds_start,
                "cds_end": cds_end,
                "strand": exon["alt_strand"],
                "chr": exon["alt_ac"],
                "gene": gene,
                "transcript": tx,
                "tx_start_i": exon["tx_start_i"],
                "tx_end_i": exon["tx_end_i"],
                "alt_start_i": exon["alt_start_i"],
                "alt_end_i": exon["alt_end_i"],
            }

            filled_exon_list.append(exon_dict)

        return_dict["exons"] = filled_exon_list
        return_dict["protein_sequence_one_letter"] = seq["one_letter_seq"]
        return_dict["protein_sequence_three_letter"] = seq["three_letter_seq"]
        return_dict["dna_sequence_full"] = seq["dna_sequence"]

        return return_dict

    def get_exons_in_range(self, exons_list, start, stop):
        return_dict = {}

        for exon in exons_list:
            if start >= exon["alt_start_i"] - 1 and start <= exon["alt_end_i"] + 1:
                return_dict["start_exon"] = exon
            if stop >= exon["alt_start_i"] - 1 and stop <= exon["alt_end_i"] + 1:
                return_dict["end_exon"] = exon

        return return_dict

    def get_sequence_for_transcript(self, tx, start=None, stop=None):
        if start and stop:
            seq = self.dataprovider.get_seq(tx, start, stop)
        else:
            seq = self.dataprovider.get_seq(tx)

        return seq

    # def get_all_transcript_info_by_transcript(self, transcript):
    #     gene_name = self.get_gene_name_by_transcript(transcript)
    #     tx_list_for_gene = self.get_transcripts(gene_name)

    #     transcript_obj = self._find_tx(tx_list_for_gene, transcript)
    #     tx_info = self.get_transcript_info(transcript)
    #     tx_exons = self.get_transcript_exons(transcript_obj)

    #     return tx_info, tx_exons

