import unittest

from src.main.hgvs_converter import HgvsConverter

class TestHgvsConverter(unittest.TestCase):
    def setUp(self):
        self.hgvs_obj_hg38 = HgvsConverter(reference_genome="GRCh38")
        self.hgvs_obj_hg19 = HgvsConverter(reference_genome="GRCh37")

    def test_get_sequence_by_gene(self):
        # Test Case 1 - Compare the first and last 20 entrys from UniProt https://www.uniprot.org/uniprotkb/P00533/entry#sequences
        test_sequence = self.hgvs_obj_hg38.get_protein_sequence("EGFR")[1]
        self.assertEqual(test_sequence[:20], "MRPSGTAGAALLALLAALCP")
        self.assertEqual(test_sequence[-20:], "TAENAEYLRVAPQSSEFIGA")

    def test_fusion_in_frame(self):
        self.assertEqual(
            self.hgvs_obj_hg19.fusion_is_inframe("chr7:47408367", "chrX:66863099"), ("In-Frame", "Out-of-Frame")
        )

    def test_genome_to_amino_hg19(self):
        # Test Case 1
        variant = "chr10:8115913C>T"

        test_key_value = {"gene_name": "GATA3", "variant_exchange": "T421M", "variant_exchange_long": "Thr421Met"}

        response = self.hgvs_obj_hg19.genome_to_amino(variant)
        for key, value in test_key_value.items():
            self.assertEqual(response[key], value)

        # Test Case 2
        variant = "chr17:7681744T>C"

        test_key_value = {"gene_name": "DNAH2", "variant_exchange": "L1833P", "variant_exchange_long": "Leu1833Pro"}

        response = self.hgvs_obj_hg19.genome_to_amino(variant)
        for key, value in test_key_value.items():
            self.assertEqual(response[key], value)

    def test_genome_to_amino_hg38(self):
        # Test Case 1 - reverse strand
        variant = "chr17:7676119C>A"

        test_key_value = {"gene_name": "TP53", "variant_exchange": "A84S", "variant_exchange_long": "Ala84Ser"}

        response = self.hgvs_obj_hg38.genome_to_amino(variant)

        for key, value in test_key_value.items():
            self.assertEqual(response[key], value)

    def test_amino_to_genome_hg19(self):
        value = [
            {
                "transcript_id": "NM_004985.5",
                "hgnc_symbol": "KRAS",
                "chromosome": "12",
                "pos_start": 25398283,
                "pos_end": 25398285,
                "strand": "-",
                "refAllele": "C",
                "varAllele": "T",
                "refCodon": "GGT",
                "varCodon": "GAT",
                "refAmino": "G",
                "ref": "G",
                "var": "D",
                "start": 12,
                "end": 12,
                "aminoacid_exchange": "G12D",
                "varAmino": "D",
                "cds_start": 190,
                "cds_end": 757,
                "prot_location": 12,
                "input_var": "KRAS:G12D",
                "c_dna_string": "NM_004985.5:c.35G>A",
                "results_string": "NC_000012.11:g.25398284C>T",
                "nucleotide_change": "GGT --> GAT|GAC",
                "query_id": "0",
                "canonical": "0",
            }
        ]
        self.assertEqual(self.hgvs_obj_hg19.amino_to_genome("KRAS:G12D"), value)

    def test_amino_to_genome_hg38(self):
        value = [
            {
                "transcript_id": "NM_004985.5",
                "hgnc_symbol": "KRAS",
                "chromosome": "12",
                "pos_start": 25245349,
                "pos_end": 25245351,
                "strand": "-",
                "refAllele": "C",
                "varAllele": "T",
                "refCodon": "GGT",
                "varCodon": "GAT",
                "refAmino": "G",
                "ref": "G",
                "var": "D",
                "start": 12,
                "end": 12,
                "aminoacid_exchange": "G12D",
                "varAmino": "D",
                "cds_start": 190,
                "cds_end": 757,
                "prot_location": 12,
                "input_var": "KRAS:G12D",
                "c_dna_string": "NM_004985.5:c.35G>A",
                "results_string": "NC_000012.12:g.25245350C>T",
                "nucleotide_change": "GGT --> GAT|GAC",
                "query_id": "0",
                "canonical": "0",
            }
        ]
        self.assertEqual(self.hgvs_obj_hg38.amino_to_genome("KRAS:G12D"), value)

    def test_amino_to_genome_indel(self):
        val = self.hgvs_obj_hg38.amino_to_genome("NC_000001.11:g.1234_2345del")
        print("indel ",val)

if __name__ == "__main__":
    unittest.main()

    # Missing Test-Cases
    # logging.info(hgvs_obj.amino_to_genome('APC:C1578S'))
    # logging.info(hgvs_obj._get_transcript_sequence("EGFR"))
    # logging.info(hgvs_obj.get_aminoacid_sequence("EGFR"))
    # logging.info(hgvs_obj.genome_to_amino('chr7:g.140753336A>T'))
    # logging.info(hgvs_obj.genome_to_amino('chr17:g.43073012A>C'))
    # logging.info(hgvs_obj.genome_to_amino('chr1:g.2488112T>A'))
    # logging.info(hgvs_obj.genome_to_amino('chr1:2488112T>A'))
    # pos = hgvs_obj.liftover_hg19_to_hg38(1, 2488112)[0][1]
    # logging.info(hgvs_obj.genome_to_amino(f'chr1:g.{pos}T>A'))
    # logging.info(type(hgvs_obj.get_transcripts("TP53")))
    # logging.info(hgvs_obj.get_transcripts("TP53"))
    # logging.info(type(hgvs_obj.amino_to_genome("KRAS:G12D")))
    # logging.info(hgvs_obj.amino_to_genome("KRAS:G12D"))
    # logging.info(hgvs_obj.amino_to_genome("KRAS:Gly12Asp"))
    # logging.info(hgvs_obj.amino_to_genome("TP53:Arg282Trp"))
    # logging.info(hgvs_obj.amino_to_genome("TP53:R282W"))
    # logging.info(hgvs_obj.amino_to_genome("NOS2:A243S"))
    # logging.info(hgvs_obj.amino_to_genome("ZNF626:V41D"))
    # logging.info(hgvs_obj.amino_to_genome("TET1:R2024L"))

    # def test_fusion_is_in_frame_hg19(self):
    #     breakpoint_a = 'chr7:47408367'
    #     breakpoint_b = 'chrX:66863099'

    #     test_key_value = {
    #         'gene_name': 'TP53',
    #         'variant_exchange': 'A84S',
    #         'variant_exchange_long': 'Ala84Ser'
    #     }

    #     response = self.hgvs_obj_hg19.fusion_is_inframe(breakpoint_a, breakpoint_b)

    #     # Gene Info 1
    #     # chr7:47408367
    #     # gene: TNS3
    #     # Triplett: CTG
    #     # Reference Aminoacid: Q
    #     # Strand: -
    #     # Breakpoint: CTG|
    #     # Exon:

    #     # Gene Info 2
    #     # chrX:66863099
    #     # gene: AR
    #     # Triplett: TTG
    #     # Reference Aminoacid: L
    #     # Strand: +
    #     # Breakpoint: T|TG
    #     # Exon:

    #     # Conclusion
    #     # Breakpoint_seq = CTG|TG
    #     # Result_long: out-of-frame
    #     # Result_short: false

    #     #response_2 = self.hgvs_obj_hg19.fusion_is_inframe("chr21:42880007","chr21:39795482")

    #     self.assertEqual(response, test_key_value)
