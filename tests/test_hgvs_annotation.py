import unittest
import adagenes


class TestHGVSAnnotation(unittest.TestCase):

    def test_transcript_request_recognition(self):
        input_var = "NM_000551.4:c.293A>G"
        request_type, groups = adagenes.get_variant_request_type(input_var)
        print(request_type)
        print(groups)
        self.assertEqual(request_type,"refseq_transcript","")

    def test_protein_request_recognition(self):
        input_var="BRAF:V600E"
        request_type, groups = adagenes.get_variant_request_type(input_var)
        print(request_type)
        print(groups)
        self.assertEqual(request_type,"gene_name_aa_exchange")
