"""
TODO
"""

import os

from hgvs_converter import HgvsConverter


def run_tests(hgvs_object):
    """
    TODO
    """
    # Test cases to check if the conversion from a genomic position to an aminoacid position is correct.
    genomic_to_amino = [
        ("NC_000017.11:g.7673776G>A", ""),
        ("NC_000017.11:g.43073012A>C", ""),
        ("NC_000001.11:g.12975309T>A", ""),
        ("NC_000001.10:g.12943048C>A", ""),
    ]

    for test_input in genomic_to_amino:
        user_input, expected_output = test_input

        hgvs_result = hgvs_object.genome_to_amino(user_input)
        calculated_output = hgvs_result["variant_exchange"]
        assert expected_output == calculated_output, f"Expected '{expected_output}' got '{calculated_output}'"

    # TODO Test cases to check if the conversion from an aminoacid position to a enomic position is correct.
    # amino_to_genomic = []

    # TODO Test cases to check if the conversion from a genomic position to an aminoacid position is correct.
    # liftover_pairs = []


if __name__ == "__main__":
    # os.environ['HGVS_SEQREPO_DIR'] = '/usr/local/share/seqrepo/2018-11-26'
    os.environ["HGVS_SEQREPO_DIR"] = "/usr/local/share/seqrepo/2021-01-29"
    os.environ["UTA_DB_URL"] = "postgresql://uta_admin:uta_admin@localhost:5432/uta/uta_20210129"

    hgvs_obj = HgvsConverter(reference_genome="GRCh37")
    run_tests(hgvs_obj)

    # TODO: Us this cases for the function above
    # Tests from VarSome
    # It would make more sense to just compare the nucleotide exchanges instead of transcripts
    # result = hgvs_obj.amino_to_genome("BRAF:V600E")
    # assert result == "NM_004333.6:c.1799T>A", f"Expected 'NM_004333.6:c.1799T>A' got '{result}'"

    # result = hgvs_obj.amino_to_genome("KRAS:G12D")
    # assert result == "NM_033360.4:c.35G>A", f"Expected 'NM_033360.4:c.35G>A' got '{result}'"

    # Not working because of selected Ensbl Transcripts
    # result = hgvs_obj.amino_to_genome("TP53:R282W")
    # assert hgvs_obj.amino_to_genome("TP53:R282W") == "ENST00000269305.4:c.844C>T", f"ENST00000269305.4:c.844C>T' " \
    #                                                                               f"got '{result}'"
    # result = hgvs_obj.amino_to_genome("TP53:R273H")
    # assert hgvs_obj.amino_to_genome("TP53:R273H") == "ENST00000269305.4:c.818G>A", f"ENST00000269305.4:c.818G>A' "

    # logging.info(hgvs_obj.amino_to_genome("TET2:K1299M"))
    # logging.info(hgvs_obj.amino_to_genome("RHOA:G17V"))
    # logging.info(hgvs_obj.amino_to_genome("ATM:L546P"))

    # logging.info(hgvs_obj.amino_to_genome("JAK1:L910P"))
    # logging.info(hgvs_obj.amino_to_genome("JAK3:A573V"))
    # logging.info(hgvs_obj.amino_to_genome("PKN1:A931V"))

    # logging.info(hgvs_obj.amino_to_genome("NRAS:G13D"))
    # logging.info(hgvs_obj.amino_to_genome("STAT3:D661Y"))
    # logging.info(hgvs_obj.amino_to_genome("NOS2:A243S"))

    # logging.info(hgvs_obj.amino_to_genome("ZNF626:V41D"))
    # logging.info(hgvs_obj.amino_to_genome("CD28:D124E"))
    # logging.info(hgvs_obj.amino_to_genome("NOTCH3:P2153L"))

    # logging.info(hgvs_obj.amino_to_genome("NOTCH1:T432M"))
    # logging.info(hgvs_obj.amino_to_genome("LAMA5:E2269K"))
    # logging.info(hgvs_obj.amino_to_genome("TET1:R2024L"))

    # test_pairs = [("BRAF:V600E", "NC_000007.14:g.140753336A>T"),
    #               ("KRAS:G12D", "NC_000012.12:g.25245350C>T"),
    #               ("TP53:R282W", "NC_000017.11:g.7673776G>A"),
    #               ("TP53:R273H", "NC_000017.11:g.7673802C>T"),
    #               # ("TET2:K1299M", ""),
    #               ]

    # for in_var, res_var in test_pairs:
    #     print(f"Test {in_var}")
    #     assert hgvs_obj.amino_to_genome(in_var) == res_var

    # hgvs_obj.get_genes_in_range(19393598, 19405164, "NC_000001.11")
    # hgvs_obj.get_genes_in_range(40850601, 40851661, "NC_000007.14")

    # a = hgvs_obj.get_transcripts('PAX8')
    # print(a)

    # a = hgvs_obj.get_transcripts('MST1')[0]
    # print(a)

    # print("All tests passed.")
