import unittest

from src.main.hgvs_converter import HgvsConverter


class TestHgvsConverterInDels(unittest.TestCase):

    def setUp(self):
        self.hgvs_obj_hg38 = HgvsConverter(reference_genome="GRCh38")
        self.hgvs_obj_hg19 = HgvsConverter(reference_genome="GRCh37")

    #def test_amino_to_genome_snv(self):
    #    val = self.hgvs_obj_hg38.genome_to_amino("chr7:140753336A>T")
    #    print("indel ", val)

    def test_amino_to_genome_del(self):
        val = self.hgvs_obj_hg38.genome_to_amino("chr9:17973354_17973355del")
        print("indel ",val)

    #def test_amino_to_genome_del_long(self):
    #    val = self.hgvs_obj_hg38.genome_to_amino("chr23:33344590_33344592del")
    #    print("indel ",val)


if __name__ == "__main__":
    unittest.main()
