import unittest, os
import main.hgvs_converter


class TestCDNAToProtein(unittest.TestCase):

    def test_cdna_to_protein(self):
        gene = "NM_000551.4"
        variant = "c.293A>G"

        SEQREPO_ENVIRON = "HGVS_SEQREPO_DIR"
        UTA_ENVIRON = "UTA_DB_URL"
        if os.environ.get(SEQREPO_ENVIRON) is None:
            os.environ[SEQREPO_ENVIRON] = "/seqrepo_data/2021-01-29"
        if os.environ.get(UTA_ENVIRON) is None:
            os.environ[UTA_ENVIRON] = "postgresql://anonymous:@uta_database:5432/uta/uta_20210129"

        converter = main.hgvs_converter.HgvsConverter()

        obj = main.hgvs_converter.CDNAtoAmino(
            converter.dataprovider, converter.assemblymapper, converter.parser, converter.validator, converter.ncbi_dictionary
        )

        result = obj.cdna_to_amino(
            gene,
            variant,
            converter.get_transcripts
        )
        print(result)

